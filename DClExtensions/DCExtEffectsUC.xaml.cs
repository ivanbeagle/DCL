﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;
using System.Diagnostics;
using DrawControlLibrary;
using System.Reflection;

namespace DClExtensions
{
    /// <summary>
    /// Contiene gli effetti draggabili sul canvas
    /// </summary>
    public partial class DCExtEffectsUC : UserControl
    {
        public event EventHandler<DCFxEventArgs> EffectChanged;

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCExtEffectsUC()
        {
            InitializeComponent();

            List<DCFxObject> fxList=new List<DCFxObject>();
                fxList.Add(new DCFxObject("DropShadowEffect", new DropShadowEffect()));
                fxList.Add(new DCFxObject("BlurEffect", new BlurEffect()));
                fxList.Add(new DCFxObject("BandedSwirlEffect",new ShaderEffectLibrary.BandedSwirlEffect()));
                fxList.Add(new DCFxObject("BloomEffect",new ShaderEffectLibrary.BloomEffect()));
                fxList.Add(new DCFxObject("BrightExtractEffect", new ShaderEffectLibrary.BrightExtractEffect()));
                fxList.Add(new DCFxObject("ColorKeyAlphaEffect", new ShaderEffectLibrary.ColorKeyAlphaEffect()));
                fxList.Add(new DCFxObject("ColorToneEffect", new ShaderEffectLibrary.ColorToneEffect()));
                fxList.Add(new DCFxObject("ContrastAdjustEffect", new ShaderEffectLibrary.ContrastAdjustEffect()));
                fxList.Add(new DCFxObject("DirectionalBlurEffect", new ShaderEffectLibrary.DirectionalBlurEffect()));
                fxList.Add(new DCFxObject("EmbossedEffect", new ShaderEffectLibrary.EmbossedEffect()));
                fxList.Add(new DCFxObject("GloomEffect", new ShaderEffectLibrary.GloomEffect()));
                fxList.Add(new DCFxObject("GrowablePoissonDiskEffect", new ShaderEffectLibrary.GrowablePoissonDiskEffect()));
                fxList.Add(new DCFxObject("InvertColorEffect", new ShaderEffectLibrary.InvertColorEffect()));
                fxList.Add(new DCFxObject("LightStreakEffect", new ShaderEffectLibrary.LightStreakEffect()));
                fxList.Add(new DCFxObject("MagnifyEffect", new ShaderEffectLibrary.MagnifyEffect()));
                fxList.Add(new DCFxObject("MonochromeEffect", new ShaderEffectLibrary.MonochromeEffect()));
                fxList.Add(new DCFxObject("PinchEffect", new ShaderEffectLibrary.PinchEffect()));
                fxList.Add(new DCFxObject("PixelateEffect", new ShaderEffectLibrary.PixelateEffect()));
                fxList.Add(new DCFxObject("RippleEffect", new ShaderEffectLibrary.RippleEffect()));
                fxList.Add(new DCFxObject("SharpenEffect", new ShaderEffectLibrary.SharpenEffect()));
                fxList.Add(new DCFxObject("SwirlEffect", new ShaderEffectLibrary.SwirlEffect()));
                fxList.Add(new DCFxObject("ToneMappingEffect", new ShaderEffectLibrary.ToneMappingEffect()));
                fxList.Add(new DCFxObject("ToonShaderEffect", new ShaderEffectLibrary.ToonShaderEffect()));
                fxList.Add(new DCFxObject("ZoomBlurEffect", new ShaderEffectLibrary.ZoomBlurEffect()));
                fxList.Add(new DCFxObject("ZoomBlurEffect", new ShaderEffectLibrary.ZoomBlurEffect()));

            _list.ItemsSource = fxList;
        }

        private void _list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DCFxObject fxItem = _list.SelectedItem as DCFxObject;
            if (fxItem == null)
                return;

            if (EffectChanged != null)
                EffectChanged(this, new DCFxEventArgs(fxItem) );
        }

        private void _list_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                // ottengo item
                DCFxObject item = _list.SelectedItem as DCFxObject;
                
                if (item == null)
                    return;

                // avvia drag                  
                DataObject dragData = new DataObject(DCDefinitions.DRAG_FORMAT_EFFECT, item.Fx);
                DragDrop.DoDragDrop(item as DCFxObject, dragData, DragDropEffects.Copy);
            } 
        }
    }

    #region Oggetti support

    public class DCFxEventArgs : EventArgs
    {
        public DCFxObject SelectedFxObj { get; internal set; }
        public DCFxEventArgs(DCFxObject fxObj) { SelectedFxObj = fxObj; }
    }

    public class DCFxObject : UIElement
    {
        public string Description { get; set; }
        public Effect Fx { get; set; }

        public DCFxObject(string descr, Effect fx)
        {
            Description = descr;
            Fx = fx;
        }
    }

    #endregion
}
