﻿using DrawControlLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DClExtensions
{
    /// <summary>
    /// Contiene la toolbar, canvas(a documenti) e la property
    /// </summary>
    public partial class DCEditor : UserControl
    {
        private int _docProgressive = 0;

        /// <summary>
        /// Ottiene il Canvas corrente
        /// </summary>
        public DrawControlLibrary.DCCanvas CurrentCanvas { get; internal set; }

        /// <summary>
        /// Ottiene il nome del documento corrente
        /// </summary>
        public string CurrentDocument { get; internal set; }

        /// <summary>
        /// Ottiene o imposta la directory delle cliparts
        /// </summary>
        public string DirectoryOfImages
        {
            get { return _clips.DirectoryOfImages; }
            set { _clips.DirectoryOfImages = value; }
        }

        /// <summary>
        /// Ottiene la lista dei documenti
        /// </summary>
        public IEnumerable<DCUserControl> Documents
        { 
            get 
            {
                List<DCUserControl> docs = new List<DCUserControl>();
                foreach(TabItem item in _tabDocs.Items.Cast<TabItem>())
                {
                    if (item.Content is DCUserControl)
                        docs.Add(item.Content as DCUserControl);
                }
                return docs;
            }
        }

        /// <summary>
        /// Ottiene o imposta il numero di documenti da creare al termine del caricamento
        /// </summary>
        public int NumberOfDocumentsAtStart { get; set; }

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCEditor()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(DCEditor_Loaded);
        }

        /// <summary>
        /// Crea e restituisce un nuovo documento
        /// </summary>
        /// <returns></returns>
        public DCUserControl AddDocument(string header = null)
        {
            // crea nuovo canvas
            DCUserControl canvasCtrl = new DCUserControl();
            canvasCtrl.Loaded += new RoutedEventHandler(_registerCanvasEvents);

            // crea tab item
            TabItem item = new TabItem();
            item.Content = canvasCtrl;
            item.Header = header != null ? header : "Disegno " + (++_docProgressive);

            // aggiunge
            int index = _tabDocs.SelectedIndex;
            _tabDocs.Items.Insert(index == -1 ? 0 : index, item);

            // abilita il documento
            CurrentCanvas = canvasCtrl.Canvas;
            CurrentDocument = item.Header.ToString();
            _palette.Canvas = CurrentCanvas;
            _palette.IsEnabled = true;

            // e lo seleziona(chiama questo metodo)
            item.IsSelected = true;

            return canvasCtrl;
        }

        // alias
        public DCUserControl AddDocument(string header, string backgroundImageFileName)
        {
            DCUserControl canvasCtrl = AddDocument(header);
            canvasCtrl.Canvas.SetBackground(backgroundImageFileName);
            return canvasCtrl;
        }

        public bool ExportAllPdf(string fileName)
        {
            return DCHelpers.ExportAllTabsAsPDF(fileName, _tabDocs);
        }

        public void SaveCurrentDocument(string fileName)
        {
            CurrentCanvas.SaveDocument(fileName);
            (_tabDocs.SelectedItem as TabItem).Header = fileName;
        }

        public void OpenDocument(string fileName)
        {
            if (CurrentCanvas.LoadDocument(fileName))
                (_tabDocs.SelectedItem as TabItem).Header = fileName;
        }

        public void EnableMenu(bool value) { _menu.Visibility = (value ? Visibility.Visible : Visibility.Collapsed); }


        private void _registerCanvasEvents(object sender, RoutedEventArgs e)
        {
            // registra eventi
            DCUserControl canvasCtrl = sender as DCUserControl;
            Debug.Assert(canvasCtrl.Canvas != null);
            if (canvasCtrl.Canvas == null)
                return;

            canvasCtrl.Canvas.SelectionChanged += new EventHandler(Canvas_SelectionChanged);
            //cCanvasCtrl.Canvas.RectangularSelection += new EventHandler<DCSelectionEventArgs>(CAD_RectangularSelection);
        }

        void Canvas_SelectionChanged(object sender, EventArgs e)
        {
            _propertyGrid.SelectObjects = CurrentCanvas.Selection.ToArray();
        }

        // si verifica quando carica l'editor
        void DCEditor_Loaded(object sender, RoutedEventArgs e)
        {
            _disableDraw();
            _addPlus();

            for (int i = 0; i < NumberOfDocumentsAtStart; i++)
                AddDocument();

            // registra evento di change sul tab control
            _tabDocs.SelectionChanged += _tabDocs_SelectionChanged;
        }


        #region Gestione TAB Control
        private void _addPlus()
        {
            // crea +

            TabItem item = new TabItem();
            item.Style = (Style)_tabDocs.TryFindResource("NewTab");
            item.Content = null;

            _tabDocs.Items.Add(item);
        }

        private void _tabDocs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInitialized)
                return;

            if (_tabDocs.SelectedIndex == _tabDocs.Items.Count - 1) // ho cliccato l'ultimo
            {
                AddDocument();
            }
            else
            {
                // recupera l'item selezionato
                TabItem item = _tabDocs.SelectedItem as TabItem;
                if (item == null)
                    return;

                string docName = item.Header.ToString();
                DCUserControl canvasCtrl = item.Content as DCUserControl;

                // abilita il documento
                CurrentCanvas = canvasCtrl.Canvas;
                CurrentDocument = item.Header.ToString();
                _palette.Canvas = CurrentCanvas;
                _palette.IsEnabled = true;
            }
        }

        // si verifica quando si rimuove un item, calcola eventualmente quello da selezionare e si preoccupa di gestire gli item
        private int _removeDocument(string docName)
        {
            if (string.IsNullOrEmpty(docName))
                return -1;

            int i=0;
            foreach(TabItem item in _tabDocs.Items)
            {
                // lo rimuove
                if (item.Header.ToString() == docName)
                {
                    // verifica se il documento corrente è quello che si cancella
                    if (CurrentDocument == docName)
                        _disableDraw();

                    item.Content = null;
                    GC.Collect();

                    // count elementi
                    int count = _tabDocs.Items.Count;

                    // indice di selezione corrente
                    int curSelectedIndex = _tabDocs.SelectedIndex;

                    // se sto eliminando il penultimo vicino al + evito che il "+" venga selezionato e seleziono l'indice precedente a quello rimosso
                    if (i == count - 2 && curSelectedIndex == count - 2)
                        _tabDocs.SelectedIndex = i - 1;

                    _tabDocs.Items.Remove(item);
                    break;
                }
                i++;
            }
            return i;
        }

        // disabilità l'utilizzo della palette e altri strumenti
        private void _disableDraw()
        {
            CurrentCanvas = null;
            CurrentDocument = null;
            _palette.IsEnabled = false;
        }

        // si verifica quando clicco sull'eliminazione di un documento
        private void x_Click(object sender, RoutedEventArgs e)
        {
            // occorre almeno un documento
            if (_tabDocs.Items.Count <= 2)
                return;

            // recupera il nome del documento e lo rimuove
            object dataCtx = (sender as Button).DataContext;
            if (dataCtx is String)
            {
                string docName = dataCtx.ToString();
                int index=_removeDocument(docName);

                if (index == _tabDocs.SelectedIndex)
                    _disableDraw();
            }
        }
        #endregion


        #region Eventi menu
        private void ExportAllPdf_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.CheckPathExists = true;
                saveDlg.ValidateNames = true;
                saveDlg.DefaultExt = ".pdf";
                saveDlg.Filter = "Documento (*.pdf)|*.pdf";

            if (saveDlg.ShowDialog().Value)
                ExportAllPdf(saveDlg.FileName);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.CheckPathExists = true;
                saveDlg.ValidateNames = true;
                saveDlg.DefaultExt = ".dc";
                saveDlg.Filter = "Disegno (*.dc)|*.dc";

            if (saveDlg.ShowDialog().Value)
                SaveCurrentDocument(saveDlg.FileName);
        }

        private void Import_Click(object sender, RoutedEventArgs e)
        {
             OpenFileDialog openDlg = new OpenFileDialog();
                openDlg.CheckPathExists = true;
                openDlg.CheckFileExists=true;
                openDlg.ValidateNames = true;
                openDlg.DefaultExt = ".dc";
                openDlg.Filter = "Disegno (*.dc)|*.dc";

            if (openDlg.ShowDialog().Value)
                CurrentCanvas.ImportDocument(openDlg.FileName, false);
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDlg = new OpenFileDialog();
                openDlg.CheckPathExists = true;
                openDlg.CheckFileExists = true;
                openDlg.ValidateNames = true;
                openDlg.DefaultExt = ".dc";
                openDlg.Filter = "Disegno (*.dc)|*.dc";

            if (openDlg.ShowDialog().Value)
                OpenDocument(openDlg.FileName);
        }

        private void Export_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.CheckPathExists = true;
                saveDlg.ValidateNames = true;
                saveDlg.DefaultExt = ".png";
                saveDlg.Filter = "Immagine (*.png)|*.png";

            if (saveDlg.ShowDialog().Value)
                CurrentCanvas.ExportImage(saveDlg.FileName);
        }

        private void ExportPdf_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
                saveDlg.CheckPathExists = true;
                saveDlg.ValidateNames = true;
                saveDlg.DefaultExt = ".pdf";
                saveDlg.Filter = "Documento (*.pdf)|*.pdf";

            if (saveDlg.ShowDialog().Value)
                CurrentCanvas.ExportPdf(saveDlg.FileName);
        }

        private void rasterSelection_Click(object sender, RoutedEventArgs e)
        {

        }

        private void groupRaster_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion
    }
}
