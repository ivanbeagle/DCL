﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DrawControlLibrary;
using DrawControlLibrary.Entities;
using System.Windows.Controls.Primitives;
using System.Diagnostics;

namespace DClExtensions
{
    /// <summary>
    /// Editor delle proprietà delle entità selezionate
    /// </summary>
    public partial class DCExtPropertiesUC : UserControl
    {
        private IDCEntity[] _selectedObjects;
        Popup _editorPopup;

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCExtPropertiesUC()
        {
            InitializeComponent();
            _editorPopup = new Popup();
        }

        /// <summary>
        /// Ottiene o imposta gli oggetti selezionati
        /// </summary>
        public IDCEntity[] SelectObjects
        {
            get
            {
                return _selectedObjects;
            }
            set
            {
                _selectedObjects = value;

                _resetProperties();

                if(_selectedObjects==null)
                    return;

                // in caso di singolo oggetto binda
                if (_selectedObjects.Count() == 1)
                {
                    IDCEntity entity = value[0];
                    _bindEntity(entity);
                }
                else
                {
                    // TODO   
                }

            }
        }




        private void _resetProperties()
        {
            _positionValue.Text = "";
            _positionValue.InputBindings.Clear();
            _positionValue.IsEnabled = false;

            _rotationValue.Text = "";
            _rotationValue.InputBindings.Clear();
            _rotationValue.IsEnabled = false;

            _scaleValue.Text = "";
            _scaleValue.InputBindings.Clear();
            _scaleValue.IsEnabled = false;

            _fillButton.Background = null;
            _fillButton.InputBindings.Clear();
            _fillButton.IsEnabled = false;

            _strokeButton.Background = null;
            _strokeButton.InputBindings.Clear();
            _strokeButton.IsEnabled = false;

            _resetPopup();
        }

        private void _resetPopup()
        {
            _editorPopup.IsOpen = false;
            _editorPopup.Child = null;
            _editorPopup.Tag = null;
            _editorPopup.PlacementTarget = null;
        }

        private void _bindEntity(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                return;

            // generico
                Binding positionBinding = new Binding("Position");
                positionBinding.Source = entity;
                positionBinding.StringFormat = "N2";
                _positionValue.SetBinding(TextBox.TextProperty, positionBinding);
                _positionValue.IsEnabled = true;

                Binding rotationBinding = new Binding("Rotation");
                rotationBinding.Source = entity;
                rotationBinding.StringFormat = "N2";
                _rotationValue.SetBinding(TextBox.TextProperty, rotationBinding);
                _rotationValue.IsEnabled = true;

                Binding scaleBinding = new Binding("Scale");
                scaleBinding.Source = entity;
                scaleBinding.StringFormat = "N2";
                _scaleValue.SetBinding(TextBox.TextProperty, scaleBinding);
                _scaleValue.IsEnabled = true;

                // elabora la destinazione di fill
                    string fillPath = "Fill";
                    string strokePath = "Stroke";
                    if (!entity.IsShape())
                    {
                        fillPath = "Background";
                        strokePath = "BorderBrush";
                    }

                    Binding fillBinding = new Binding(fillPath);
                    fillBinding.Source = entity;
                    _fillButton.Tag = entity;
                    _fillButton.SetBinding(Button.BackgroundProperty, fillBinding);
                    _fillButton.IsEnabled = true;

                    Binding strokeBinding = new Binding(strokePath);
                    strokeBinding.Source = entity;
                    _strokeButton.Tag = entity;
                    _strokeButton.SetBinding(Button.BackgroundProperty, strokeBinding);
                    _strokeButton.IsEnabled = true;
        }


        #region Logica Brushes
        void brushEditor_fillBrushChanged(object sender, Lovatts.ColorEditor.BrushEditorEventArgs e)
        {
            IDCEntity entity = _fillButton.Tag as IDCEntity;
            if (entity is DCShape)
            {
                DCShape shape = entity as DCShape;
                shape.Fill = e.Brush;
            }
        }

        void brushEditor_strokeBrushChanged(object sender, Lovatts.ColorEditor.BrushEditorEventArgs e)
        {
            IDCEntity entity = _strokeButton.Tag as IDCEntity;
            if (entity is DCShape)
            {
                DCShape shape = entity as DCShape;
                shape.Stroke = e.Brush;
            }
        }

        private void _strokeButton_Click(object sender, RoutedEventArgs e)
        {
            if (_editorPopup.PlacementTarget == _strokeButton)
            {
                _resetPopup();
                return;
            }

            Lovatts.ColorEditor.BrushEditor brushEditor = new Lovatts.ColorEditor.BrushEditor();
            brushEditor.BrushEditorViewModel.Brush = _strokeButton.Background;
            brushEditor.BrushChanged += new EventHandler<Lovatts.ColorEditor.BrushEditorEventArgs>(brushEditor_strokeBrushChanged);

            _editorPopup.Child = brushEditor;
            _editorPopup.PlacementTarget = _strokeButton;
            _editorPopup.IsOpen = true;
        }

        private void _fillButton_Click(object sender, RoutedEventArgs e)
        {
            if (_editorPopup.PlacementTarget == _fillButton)
            {
                _resetPopup();
                return;
            }

            Lovatts.ColorEditor.BrushEditor brushEditor = new Lovatts.ColorEditor.BrushEditor();
            brushEditor.BrushEditorViewModel.Brush = _fillButton.Background;
            brushEditor.BrushChanged += new EventHandler<Lovatts.ColorEditor.BrushEditorEventArgs>(brushEditor_fillBrushChanged);

            _editorPopup.Child = brushEditor;
            _editorPopup.PlacementTarget = _fillButton;
            _editorPopup.IsOpen = true;
        }
        #endregion
    
    
    }
}
