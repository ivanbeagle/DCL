﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;
using System.Diagnostics;
using DrawControlLibrary;
using System.Reflection;
using System.Collections.ObjectModel;
using System.IO;

namespace DClExtensions
{
    /// <summary>
    /// Mostra l'elenco di immagini
    /// </summary>
    public partial class DCClipsUC : UserControl
    {
        /// <summary>
        /// Ottiene o imposta il path delle immagini
        /// </summary>
        public string DirectoryOfImages
        {
            get { return (string)GetValue(DirectoryOfImagesProperty); }
            set { SetValue(DirectoryOfImagesProperty, value); }
        }
        public static readonly DependencyProperty DirectoryOfImagesProperty = DependencyProperty.Register("DirectoryOfImages", typeof(string), typeof(DCClipsUC), new PropertyMetadata("", new PropertyChangedCallback(_onDirectoryOfImagesChange)));
        private static void _onDirectoryOfImagesChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DCClipsUC uc = d as DCClipsUC;
            string path = e.NewValue.ToString();

            uc._getClipsFrom(path);
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCClipsUC()
        {
            InitializeComponent();

            _getClipsFrom(DirectoryOfImages);
        }

        private void _getClipsFrom(string path)
        {
            if (!(path != null && Directory.Exists(path)))
                return;

            ObservableCollection<DCClipObject> listOfClips = new ObservableCollection<DCClipObject>();

            // ottiene i file
            IEnumerable<string> files = Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly).Where(f => f.EndsWith(".jpg") || f.EndsWith(".png"));
            foreach (string filename in files)
            {
                DCClipObject clipObj = new DCClipObject(filename);
                listOfClips.Add(clipObj);
            }

            // aggiorna dati
            _list.ItemsSource = listOfClips;
            UpdateLayout();
        }

        private void _list_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                // ottengo item
                DCClipObject item = _list.SelectedItem as DCClipObject;
                
                if (item == null)
                    return;

                // avvia drag                  
                DataObject dragData = new DataObject(DCDefinitions.DRAG_FORMAT_CLIP, item);
                DragDrop.DoDragDrop(item, dragData, DragDropEffects.Copy);
            } 
        }
    }


    #region Oggetti support
    public class DCClipObject : UIElement
    {
        public string Filename { get; set; }
        public BitmapSource ImageClip { get; set; }

        public DCClipObject(string filename)
        {
            Filename = filename;
            try { ImageClip = new BitmapImage(new Uri(Filename, UriKind.RelativeOrAbsolute)); }
            catch { }
        }
    }
    #endregion
}
