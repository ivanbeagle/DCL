﻿using DrawControlLibrary.Tools;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DrawControlLibrary
{
    /// <summary>
    /// UserControl composto e minimale del canvas che provvede zoom, coordinate e notifiche
    /// </summary>
    public partial class DCUserControl : UserControl
    {
        public DCCanvas Canvas { get { return _canvas; } }

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCUserControl()
        {
            InitializeComponent();

            // notificare alle labels
                _canvas.NotifyCoordinatesTo(_coordinates);
                _canvas.NotifySheetSizeTo(_sheetSize);

            _canvas.UpdateSheetBox();
        }

        private void _fillBtn_Click(object sender, RoutedEventArgs e)
        {
            _slider.Value = 1;
        }

        private void _canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
                _slider.Value += 0.1;
            else
                _slider.Value -= 0.1;
        }


        public double Zoom
        {
            get { return _slider.Value; }
            set { _slider.Value = value; }
        }

        /// <summary>
        /// Ottiene o imposta il controllo visuale dello zoom
        /// </summary>
        public bool ZoomControllerEnabled
        {
            get { return _panelZoomController.Visibility == System.Windows.Visibility.Visible; }
            set
            {
                _panelZoomController.Visibility = value ? Visibility.Visible : Visibility.Hidden;
            }
        }

        private void _measureBtn_Click(object sender, RoutedEventArgs e)
        {
            _canvas.SetTool(new DCMeasureTool());
        }
    }
}
