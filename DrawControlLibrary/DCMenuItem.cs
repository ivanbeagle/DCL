﻿using System;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using DrawControlLibrary.Managers;

namespace DrawControlLibrary
{ 
    /// <summary>
    /// Aggiunge proprietà e funzionalità al menu item (per ora di uso interno)
    /// </summary>
    internal class DCMenuItem : MenuItem
    {
        internal ContextMenu _menu;
        internal DCContextualDelegate _command;
        internal bool _orderSelection;
        internal bool _selectionAsc;

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="caption"></param>
        /// <param name="command"></param>
        /// <param name="menu"></param>
        /// <param name="iconPath"></param>
        /// <param name="orderSelection"></param>
        /// <param name="selectionAsc"></param>
        internal DCMenuItem(string caption, DCContextualDelegate command, ContextMenu menu, string iconPath="", bool orderSelection=true, bool selectionAsc=true)
        {
            // fa le varie validazioni
            Debug.Assert(!String.IsNullOrEmpty(caption));
            if (String.IsNullOrEmpty(caption))
                throw new ArgumentNullException("caption");

            Debug.Assert(command != null);
            if (command == null)
                throw new ArgumentNullException("command");

            Debug.Assert(menu != null);
            if (menu == null)
                throw new ArgumentNullException("menu");

            // memorizza informazioni
            Header = caption;
            if (!String.IsNullOrEmpty(iconPath))
            {
                Image image = new Image();
                image.Source = new BitmapImage(new Uri(iconPath));
                this.Icon = image;
            }
            _command = command;
            _menu = menu;
            _orderSelection = orderSelection;
            _selectionAsc = selectionAsc;

            // comportamento
            this.Click += new System.Windows.RoutedEventHandler(DCCommandContextMngr.OnMenuItemClicked);
            _menu.Items.Add(this);
        }
    }
}
