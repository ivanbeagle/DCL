﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Data;
using System.Globalization;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Adorners
{
    // Supporto all'adorner visuale per oggetti DCHost (codice originale di DiagramDesigner modificato per l'esigenze)
    
    internal class ResizeRotateAdorner : Adorner
    {
        private VisualCollection _visuals;
        private ResizeRotateChrome _chrome;

        protected override int VisualChildrenCount { get { return this._visuals.Count; } }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="designerItem"></param>
        public ResizeRotateAdorner(DCHost designerItem) : base(designerItem)
        {
            SnapsToDevicePixels = true;
            this._chrome = new ResizeRotateChrome();
            this._chrome.DataContext = designerItem;
            this._visuals = new VisualCollection(this);
            this._visuals.Add(this._chrome);
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            this._chrome.Arrange(new Rect(arrangeBounds));
            return arrangeBounds;
        }

        protected override Visual GetVisualChild(int index)
        {
            return this._visuals[index];
        }
    }

    internal class ResizeRotateChrome : Control
    {
        static ResizeRotateChrome()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(ResizeRotateChrome), new FrameworkPropertyMetadata(typeof(ResizeRotateChrome)));
        }
    }

    internal class SizeAdorner : Adorner
    {
        //private SizeChrome chrome;
        private VisualCollection _visuals;
        private DCHost _designerItem;

        protected override int VisualChildrenCount { get { return this._visuals.Count; } }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="designerItem"></param>
        public SizeAdorner(DCHost designerItem) : base(designerItem)
        {
            this.SnapsToDevicePixels = true;
            this._designerItem = designerItem;
            //this.chrome = new SizeChrome();
            //this.chrome.DataContext = designerItem;
            this._visuals = new VisualCollection(this);
            //this.visuals.Add(this.chrome);
        }

        protected override Visual GetVisualChild(int index)
        {
            return this._visuals[index];
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            //this.chrome.Arrange(new Rect(new Point(0.0, 0.0), arrangeBounds));
            return arrangeBounds;
        }
    }

    internal class RotateThumb : Thumb
    {
        private double _initialAngle;
        private RotateTransform _rotateTransform;
        private Vector _startVector;
        private Point _centerPoint;
        private DCHost _designerItem;
        private DCCanvas _canvas;

        /// <summary>
        /// Costruttore
        /// </summary>
        public RotateThumb()
        {
            DragDelta += new DragDeltaEventHandler(this.RotateThumb_DragDelta);
            DragStarted += new DragStartedEventHandler(this.RotateThumb_DragStarted);
        }

        private void RotateThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            this._designerItem = DataContext as DCHost;

            if (this._designerItem != null)
            {
                this._canvas = this._designerItem.GetCanvas();

                if (this._canvas != null)
                {
                    this._centerPoint = this._designerItem.TranslatePoint
                    (
                        new Point(this._designerItem.ActualWidth * this._designerItem.RenderTransformOrigin.X, // era Width
                                  this._designerItem.ActualHeight * this._designerItem.RenderTransformOrigin.Y), // era Height
                                  this._canvas
                    );

                    Point startPoint = Mouse.GetPosition(this._canvas);
                    this._startVector = Point.Subtract(startPoint, this._centerPoint);
                    this._rotateTransform = this._designerItem.RotationTransform;
                    this._initialAngle = this._rotateTransform.Angle;
                }
            }
        }

        private void RotateThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (this._designerItem != null && this._canvas != null)
            {
                Point currentPoint = Mouse.GetPosition(this._canvas);
                Vector deltaVector = Point.Subtract(currentPoint, this._centerPoint);

                double angle = Vector.AngleBetween(this._startVector, deltaVector);

                RotateTransform rotateTransform = this._designerItem.RotationTransform;
                rotateTransform.Angle = this._initialAngle + Math.Round(angle, 0);
                this._designerItem.InvalidateMeasure();
            }
        }
    }

    internal class ResizeThumb : Thumb
    {
        private RotateTransform _rotateTransform;
        private double _angle;
        private Adorner _adorner;
        private Point _transformOrigin;
        private DCHost _designerItem;
        private DCCanvas _canvas;

        /// <summary>
        /// Costruttore
        /// </summary>
        public ResizeThumb()
        {
            DragStarted += new DragStartedEventHandler(this.ResizeThumb_DragStarted);
            DragDelta += new DragDeltaEventHandler(this.ResizeThumb_DragDelta);
            DragCompleted += new DragCompletedEventHandler(this.ResizeThumb_DragCompleted);
        }

        private void ResizeThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            this._designerItem = this.DataContext as DCHost;

            if (this._designerItem != null)
            {
                this._canvas = this._designerItem.GetCanvas();

                if (this._canvas != null)
                {
                    this._transformOrigin = this._designerItem.RenderTransformOrigin;

                    this._rotateTransform = this._designerItem.RotationTransform;
                    if (this._rotateTransform != null)
                    {
                        this._angle = this._rotateTransform.Angle * Math.PI / 180.0;
                    }
                    else
                    {
                        this._angle = 0.0d;
                    }

                    AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this._canvas);
                    if (adornerLayer != null)
                    {
                        this._adorner = new SizeAdorner(this._designerItem);
                        adornerLayer.Add(this._adorner);
                    }
                }
            }
        }

        private void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (this._designerItem != null)
            {
                double deltaVertical, deltaHorizontal;

                switch (VerticalAlignment)
                {
                    case System.Windows.VerticalAlignment.Bottom:
                        deltaVertical = Math.Min(-e.VerticalChange, this._designerItem.ActualHeight - this._designerItem.MinHeight);
                        DCCanvas.SetTop(this._designerItem, DCCanvas.GetTop(this._designerItem) + (this._transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this._angle))));
                        DCCanvas.SetLeft(this._designerItem, DCCanvas.GetLeft(this._designerItem) - deltaVertical * this._transformOrigin.Y * Math.Sin(-this._angle));
                        this._designerItem.Height = this._designerItem.ActualHeight - deltaVertical;
                        break;
                    case System.Windows.VerticalAlignment.Top:
                        deltaVertical = Math.Min(e.VerticalChange, this._designerItem.ActualHeight - this._designerItem.MinHeight);
                        DCCanvas.SetTop(this._designerItem, DCCanvas.GetTop(this._designerItem) + deltaVertical * Math.Cos(-this._angle) + (this._transformOrigin.Y * deltaVertical * (1 - Math.Cos(-this._angle))));
                        DCCanvas.SetLeft(this._designerItem, DCCanvas.GetLeft(this._designerItem) + deltaVertical * Math.Sin(-this._angle) - (this._transformOrigin.Y * deltaVertical * Math.Sin(-this._angle)));
                        this._designerItem.Height = this._designerItem.ActualHeight - deltaVertical;
                        break;
                    default:
                        break;
                }

                switch (HorizontalAlignment)
                {
                    case System.Windows.HorizontalAlignment.Left:
                        deltaHorizontal = Math.Min(e.HorizontalChange, this._designerItem.ActualWidth - this._designerItem.MinWidth);
                        DCCanvas.SetTop(this._designerItem, DCCanvas.GetTop(this._designerItem) + deltaHorizontal * Math.Sin(this._angle) - this._transformOrigin.X * deltaHorizontal * Math.Sin(this._angle));
                        DCCanvas.SetLeft(this._designerItem, DCCanvas.GetLeft(this._designerItem) + deltaHorizontal * Math.Cos(this._angle) + (this._transformOrigin.X * deltaHorizontal * (1 - Math.Cos(this._angle))));
                        this._designerItem.Width = this._designerItem.ActualWidth - deltaHorizontal;
                        break;
                    case System.Windows.HorizontalAlignment.Right:
                        deltaHorizontal = Math.Min(-e.HorizontalChange, this._designerItem.ActualWidth - this._designerItem.MinWidth);
                        DCCanvas.SetTop(this._designerItem, DCCanvas.GetTop(this._designerItem) - this._transformOrigin.X * deltaHorizontal * Math.Sin(this._angle));
                        DCCanvas.SetLeft(this._designerItem, DCCanvas.GetLeft(this._designerItem) + (deltaHorizontal * this._transformOrigin.X * (1 - Math.Cos(this._angle))));
                        this._designerItem.Width = this._designerItem.ActualWidth - deltaHorizontal;
                        break;
                    default:
                        break;
                }
            }

            e.Handled = true;
        }

        private void ResizeThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (this._adorner != null)
            {
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this._canvas);
                if (adornerLayer != null)
                {
                    adornerLayer.Remove(this._adorner);
                }

                this._adorner = null;
            }
        }
    }
}
