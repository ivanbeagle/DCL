﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegno rettangoli
    /// </summary>
    public class DCRectangleTool : DCAbstractTool
    {
        int step = 0;

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.ShapeType = DCShapeType.Rectangle;
            Canvas.SetPrompt("Primo punto");
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            if (step == 0)
            {
                Canvas.ToolShape_1.Position = Mouse.GetPosition(Canvas);
                Canvas.SetPrompt("Secondo punto");
            }

            else if (step == 1)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                DCShape rect = new DCShape();
                    rect.ShapeType = DCShapeType.Rectangle;
                    rect.Position = Canvas.ToolShape_1.Position.GetClone();
                    rect.Points.Add(new Point(0, 0));
                    rect.Points.Add(Canvas.ToolShape_1.Points[1].GetClone());
                    Canvas.AddEntity(rect);

                    // rifa il setup
                    Canvas.Children.Remove(Canvas.ToolShape_1);
                    Canvas.Children.Add(Canvas.ToolShape_1);
                    DCToolHelper.SetupShape(Canvas.ToolShape_1);
                    Canvas.ToolShape_1.ShapeType = DCShapeType.Rectangle;

                    // refresh del prompt
                    Canvas.RefreshPrompt();
                    Canvas.SetPrompt("Primo punto");

                step = 0;
                return;
            }

            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step == 1)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Points[1] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);
            }
        }
    }
}
