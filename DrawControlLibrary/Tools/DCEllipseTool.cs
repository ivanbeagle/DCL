﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegno ellissi
    /// </summary>
    public class DCEllipseTool : DCAbstractTool
    {
        int step = 0;

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.ShapeType = DCShapeType.Ellipse;
            Canvas.SetPrompt("Punto centrale");
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            if (step == 0)
            {
                Canvas.ToolShape_1.Position = Mouse.GetPosition(Canvas);
                Canvas.SetPrompt("Raggio");
            }

            else if (step == 1)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);

                DCShape ellipse = new DCShape();
                    ellipse.ShapeType = DCShapeType.Ellipse;
                    ellipse.Position = Canvas.ToolShape_1.Position.GetClone();
                    ellipse.Points.Add(new Point(0, 0));
                    ellipse.Points.Add(Canvas.ToolShape_1.Points[1].GetClone());
                    Canvas.AddEntity(ellipse);

                    // rifa il setup
                    Canvas.Children.Remove(Canvas.ToolShape_1);
                    Canvas.Children.Add(Canvas.ToolShape_1);
                    DCToolHelper.SetupShape(Canvas.ToolShape_1);
                    Canvas.ToolShape_1.ShapeType = DCShapeType.Ellipse;

                    // refresh del prompt
                    Canvas.RefreshPrompt();
                    Canvas.SetPrompt("Punto centrale");

                step = 0;
                return;
            }

            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step == 1)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Points[1] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);
            }
        }
    }
}
