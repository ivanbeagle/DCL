﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;
using System.Windows.Media;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegno frecce
    /// </summary>
    public class DCArrowTool : DCAbstractTool
    {
        int step = 0;

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.Fill = Brushes.Black;
            Canvas.ToolShape_1.ShapeType = DCShapeType.Arrow;
            Canvas.SetPrompt("Primo punto");
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            Point mouse = Mouse.GetPosition(Canvas);

            if (step == 0)
            {
                Canvas.ToolShape_1.SetStartPoint(mouse);
                Canvas.SetPrompt("Secondo punto");
            }

            else if (step == 1)
            {
                Canvas.ToolShape_1.SetFromAbsolutePoint(1, mouse);
                Canvas.SetPrompt("Dimensioni freccia");
                Canvas.ToolShape_1.Points.Add(Canvas.ToolShape_1.Points[step].GetClone());
            }

            else if (step == 2)
            {
                // setta l'ultimo punto e aggiunge
                Canvas.ToolShape_1.SetFromAbsolutePoint(2, mouse);

                // aggiunge freccia
                DCShape arrow = new DCShape();
                    arrow.ShapeType = DCShapeType.Arrow;
                    arrow.Position = Canvas.ToolShape_1.Position.GetClone();
                    arrow.Points.Add(DCUtils.ZeroPt());
                    arrow.Points.Add(Canvas.ToolShape_1.Points[1].GetClone());
                    arrow.Points.Add(Canvas.ToolShape_1.Points[2].GetClone());
                    arrow.Fill = Brushes.Black;
                    Canvas.AddEntity(arrow);

                // refresh prompt
                Canvas.RefreshPrompt();
                Canvas.SetPrompt("Primo punto");

                // rifa il setup
                Canvas.Children.Remove(Canvas.ToolShape_1);
                Canvas.Children.Add(Canvas.ToolShape_1);
                DCToolHelper.SetupShape(Canvas.ToolShape_1);
                Canvas.ToolShape_1.ShapeType = DCShapeType.Arrow;
                Canvas.ToolShape_1.Fill = Brushes.Black;

                step = 0;
                return;
            }
            
            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step > 0)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Points[step] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);
            }
        }
    }
}
