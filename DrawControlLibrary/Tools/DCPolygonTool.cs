﻿using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Tools
{
    /// <summary>
    /// Strumento di disegno poligoni
    /// </summary>
    public class DCPolygonTool : DCAbstractTool
    {
        private int step = 0;
        protected DCShapeType _shapeType;

        /// <summary>
        /// Costruttore
        /// </summary>
        public DCPolygonTool()
        {
            _shapeType = DCShapeType.CurvedPolygon;
        }

        public override void Start(object param = null)
        {
            base.Start(param);

            DCToolHelper.SetupShape(Canvas.ToolShape_1);
            Canvas.ToolShape_1.Smooth = 0;
            Canvas.ToolShape_1.ShapeType = _shapeType;
            Canvas.SetPrompt("Primo punto");
        }

        public override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);

            if (e.Key == Key.Return)
            {
                if (Canvas.ToolShape_1.Points.Count > 2)
                {
                    // aggiunge l'oggetto
                    DCShape polygon = new DCShape();
                        polygon.ShapeType = _shapeType;
                        polygon.Smooth = 0;
                        polygon.Position = Canvas.ToolShape_1.Position.GetClone();
                        foreach (Point p in Canvas.ToolShape_1.Points)
                            polygon.Points.Add(p.GetClone());
                        polygon.Points.RemoveAt(polygon.Points.Count - 1);

                    Canvas.AddEntity(polygon);
                }

                // rifa il setup
                Canvas.Children.Remove(Canvas.ToolShape_1);
                Canvas.Children.Add(Canvas.ToolShape_1);
                DCToolHelper.SetupShape(Canvas.ToolShape_1);
                Canvas.ToolShape_1.ShapeType = _shapeType;

                // refresh del prompt
                Canvas.RefreshPrompt();
                Canvas.SetPrompt("Primo punto");

                step = 0;
            }
        }

        public override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            if (IsAbort)
                return;

            if (step == 0)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Position = mouse.GetClone();
                Canvas.ToolShape_1.Points[0] = new Point(0, 0);
                Canvas.SetPrompt("Secondo punto");
            }
            else if (step > 0)
            {
                Canvas.SetPrompt("Punti successivi (INVIO chiude)");
                Canvas.ToolShape_1.Points.Add(Canvas.ToolShape_1.Points[step].GetClone());
            }

            step++;
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (step > 0)
            {
                Point mouse = Mouse.GetPosition(Canvas);
                Canvas.ToolShape_1.Points[step] = new Point(mouse.X - Canvas.ToolShape_1.Position.X, mouse.Y - Canvas.ToolShape_1.Position.Y);
            }
        }
    }
}
