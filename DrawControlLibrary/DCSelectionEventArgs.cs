﻿using System;
using System.Drawing;

namespace DrawControlLibrary
{
    /// <summary>
    /// Struttura EventArgs che contiene informazioni sulla selezione
    /// </summary>
    public class DCSelectionEventArgs : EventArgs
    {
        DCSelectionType _selectionType;
        public DCSelectionType SelectionType { get { return _selectionType; } }

        RectangleF _rectArea;
        public RectangleF RectangularArea { get { return _rectArea; } }

        public DCSelectionEventArgs(DCSelectionType selectionType)
        {
            _selectionType = selectionType;
        }

        public DCSelectionEventArgs(DCSelectionType selectionType, RectangleF rectArea)
        {
            _selectionType = selectionType;
            _rectArea = rectArea;
        }
    }
}
