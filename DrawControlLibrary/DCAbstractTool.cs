﻿using System.Windows.Input;
using System.Windows.Media;

namespace DrawControlLibrary
{
    /// <summary>
    /// Classe di base per la creazione di tools
    /// </summary>
    public abstract class DCAbstractTool
    {
        internal    DCCanvas _canvas;

        public DCCanvas Canvas              { get { return _canvas; } }
        public bool     IsFinished          { get; set; }
        public bool     IsAbort             { get; set; }
        public bool     AllowInteraction    { get; set; }

        public object   StartParam          { get; set; }

        public virtual void Start(object param=null)
        {
            StartParam = param;

            // setup
                if(!_canvas.Children.Contains(_canvas.ToolShape_1))
                    _canvas.Children.Add(_canvas.ToolShape_1);
                if (!_canvas.Children.Contains(_canvas.ToolShape_2))
                    _canvas.Children.Add(_canvas.ToolShape_2);
                if (!_canvas.Children.Contains(_canvas.ToolShape_3))
                    _canvas.Children.Add(_canvas.ToolShape_3);
                if (!_canvas.Children.Contains(_canvas.ToolShape_4))
                    _canvas.Children.Add(_canvas.ToolShape_4);
                if (!_canvas.Children.Contains(_canvas.ToolShape_5))
                    _canvas.Children.Add(_canvas.ToolShape_5);
                if (!_canvas.Children.Contains(_canvas.Prompt))
                    _canvas.Children.Add(_canvas.Prompt);

            IsFinished  = false;
            IsAbort     = false;

            if (!AllowInteraction)
                Canvas.Cursor = Cursors.Cross;
        }

        public virtual void Finish() /*object endParam=null*/
        {
            _canvas.Children.Remove(_canvas.ToolShape_1);
            _canvas.Children.Remove(_canvas.ToolShape_2);
            _canvas.Children.Remove(_canvas.ToolShape_3);
            _canvas.Children.Remove(_canvas.ToolShape_4);
            _canvas.Children.Remove(_canvas.ToolShape_5);
            _canvas.Children.Remove(_canvas.Prompt);
            
            IsFinished = true;

            Canvas.Cursor = Cursors.Arrow;
        }

        public virtual void OnMouseDown(MouseButtonEventArgs e)
        {
        }

        public virtual void OnMouseUp(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
                IsAbort = true;
        }

        public virtual void OnMouseMove(MouseEventArgs e)
        {
        }

        public virtual void OnKeyDown(KeyEventArgs e)
        {
        }

        public virtual void OnKeyUp(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                IsAbort = true;
        }

        public virtual void OnRender(DrawingContext dc)
        {
        }

        public virtual void OnAbort()
        {
        }
    }
}
