﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using DrawControlLibrary.Managers;

namespace DrawControlLibrary
{
    /// <summary>
    /// Delegato di disegno entità
    /// </summary>
    /// <param name="dc"></param>
    public delegate void RenederDelegate(DrawingContext dc);

    /// <summary>
    /// Interfaccia di entità generica
    /// </summary>
    public interface IDCEntity : ICloneable
    {
        // proprietà
        string Id { get; set; }
        Point Position { get; set; }
        DCMoveableType MoveableType { get; set; }
        DCAbstractControlPointMngr ControlPointsManager { get; set; }
        bool Editable { get; set; }
        bool Deletable { get; set; }
        bool Selectable { get; set; }
        bool IsSystemEntity { get; set; }
        bool Selected { get; set; }
        Point Scale { get; set; }
        Dictionary<string, object> Data { get; set; }

        // eventi
        event EventHandler MoveEvent;
        event EventHandler<DCEditEventArgs> EditEvent;
        event EventHandler<DCSelectionEventArgs> SelectedEvent;
        event EventHandler<DCSelectionEventArgs> UnSelectedEvent;
        event RenederDelegate RenderEvent;

        // metodi
        DCCanvas GetCanvas();
        Point GetCenter();
        System.Drawing.RectangleF GetBounds();

        string _getSerialization(object addictionalParam=null);

        // notificatori
        void _notifyMoveEvent();
        void _notifyEditEvent(int pointIndex);
        void _notifySelectedEvent(DCSelectionType type);
        void _notifyUnSelectedEvent(DCSelectionType type);
    }
}
