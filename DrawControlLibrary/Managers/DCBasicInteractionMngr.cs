﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Manager per il drag degli elementi grafici
    /// </summary>
    internal static class DCBasicInteractionMngr
    {
        internal static void OnMouseDown(IDCEntity entity, MouseButtonEventArgs e)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupera canvas
            DCCanvas canvas = entity.GetCanvas();

            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // notifico cattura dell'entità e nuovo posizionamento del mouse
            entity.Data[DCDefinitions.KEY_CAPTURED] = true;
            entity.Data[DCDefinitions.KEY_OLD_MOUSE_POSITION] = e.GetPosition(canvas);
        }

        internal static void OnMouseUp(IDCEntity entity, MouseButtonEventArgs e)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // notifico flags
            entity.Data[DCDefinitions.KEY_SYS_DRAGGING] = entity.Data[DCDefinitions.KEY_DRAGGING];
            entity.Data[DCDefinitions.KEY_CAPTURED] = false;
            entity.Data[DCDefinitions.KEY_DRAGGING] = false;

            // verifica elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // invalida elemento
            element.InvalidateVisual();

            // invalido le misure quando rilascio l'oggetto per aggiornare sempre il bounding
            element.InvalidateMeasure();            
        }

        internal static void OnMouseMove(IDCEntity entity, MouseEventArgs e)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupera il canvas e verifica
            DCCanvas canvas = entity.GetCanvas();

            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // se l'elemento è catturato
            if ((bool)entity.Data[DCDefinitions.KEY_CAPTURED] == true)
            {
                // notifico lo stato di drag
                entity.Data[DCDefinitions.KEY_DRAGGING] = true;

                // calcolo nuovo posizionamento
                Point mouse = e.GetPosition(canvas);
                Point oldMousePosition = (Point)entity.Data[DCDefinitions.KEY_OLD_MOUSE_POSITION];
                double xDiff = mouse.X - oldMousePosition.X;
                double yDiff = mouse.Y - oldMousePosition.Y;

                Point position = entity.Position;
                double left = position.X;
                double top = position.Y;
                left = double.IsNaN(left) ? 0 : left;
                top = double.IsNaN(top) ? 0 : top;

                // verifica vincolo
                switch (entity.MoveableType)
                {
                    case DrawControlLibrary.DCMoveableType.NoMoveable:
                        return;

                    case DrawControlLibrary.DCMoveableType.MoveableHorizontal:
                        entity.Position = new Point(left + xDiff, top);
                        break;

                    case DrawControlLibrary.DCMoveableType.MoveableVertical:
                        entity.Position = new Point(left, top + yDiff);
                        break;

                    case DrawControlLibrary.DCMoveableType.MoveableFree:
                        entity.Position = new Point(left + xDiff, top + yDiff);
                        break;
                }

                // deve aggiornare la posizione del mouse relativa all'ultimo punto toccato
                entity.Data[DCDefinitions.KEY_OLD_MOUSE_POSITION] = mouse;

                // notifica movimento
                entity._notifyMoveEvent();
            }
        }
    }
}
