﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe manager per la gestione dei punti di controllo di un entità host (OBSOLETA)
    /// </summary>
    public class DCHostControlPointsMngr : DCAbstractControlPointMngr
    {
        public DCHostControlPointsMngr(IDCEntity entity) : base(entity) { }

        // calcola la nuova coordinata e la assegna al punto di controllo
        private void _onUpdate(DCControlPoint cp)
        {
            //// recupera entità
            //DCHost host = _entity as DCHost;

            //// recupera posizione
            //Point position = _entity.Position;

            //// assume la dimensione del corner in basso a destra
            //DCCanvas.SetLeft(cp, position.X + host.ActualWidth);
            //DCCanvas.SetTop(cp, position.Y + host.ActualHeight);
        }

        // aggiunge i punti di controllo al canvas
        public override void AddPoints()
        {
            base.AddPoints();

            //// recupera canvas
            //DCCanvas canvas = _entity.GetCanvas();

            //// valida canvas
            //Debug.Assert(canvas != null);
            //if (canvas == null)
            //    throw new ArgumentNullException("canvas");

            //// crea il punto
            //DCControlPoint cp = new DCControlPoint(_entity, 0);
            //    cp.Cursor = Cursors.SizeNWSE;
            
            //// assegna la posizione
            //_onUpdate(cp);

            //// aggiunge al canvas
            //canvas.Children.Add(cp);
        }

        // si verifica quando cambia la posizione
        public override void Update()
        {
            //// recupera entità
            //DCHost host = _entity as DCHost;

            //// recupera canvas
            //DCCanvas canvas = _entity.GetCanvas();

            //// valida canvas
            //Debug.Assert(canvas != null);
            //if (canvas == null)
            //    throw new ArgumentNullException("canvas");

            //// aggiorna il punto(per ora uno)
            //foreach (DCControlPoint cp in canvas._cacheControlPoints)
            //{
            //    if (cp._entity == _entity)
            //    {
            //        _onUpdate(cp);
            //        return;
            //    }
            //}
        }
    }
}
