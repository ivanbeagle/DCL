﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Delegato per azioni contestuali
    /// </summary>
    /// <param name="entity"></param>
    public delegate void DCContextualDelegate(IDCEntity entity);

    /// <summary>
    /// Manager che gestisce il menu contestuale
    /// </summary>
    public static class DCCommandContextMngr
    {
        /// <summary>
        /// Esegue il comando contestuale specificato per l'entità selezionata. Se c'è una selezione attiva applica il comando all'intera selezione ordinata
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="method"></param>
        /// <param name="entity"></param>
        /// <param name="orderSelection"></param>
        /// <param name="asc"></param>
        public static void ExecuteContestualCommand(DCCanvas canvas, DCContextualDelegate method, IDCEntity entity = null, bool orderSelection = true, bool asc = true)
        {
            try
            {
                // validazione necessaria
                Debug.Assert(canvas != null);
                if (canvas == null)
                    throw new ArgumentNullException("canvas");

                Debug.Assert(method != null);
                if (method == null)
                    throw new ArgumentNullException("method");

                // ottiene una copia della selezione ordinata senza variare la selezione originale
                var listSelection = orderSelection == true ? canvas.GetOrderSelection(asc) : canvas.Selection.ToArray();

                bool lockOnSelectedEntity = false;

                // esegue il delegato sulla selezione
                foreach (IDCEntity selectedEntity in listSelection)
                {
                    // se l'entità compare nella selezione farà in modo che non verrà eseguito 2 volte sulla stessa!
                    if (selectedEntity == entity)
                        lockOnSelectedEntity = true;

                    method(selectedEntity);
                }

                // se l'entità è selezionata esegue anche sull'entità(a patto che non sia già stata eseguita precedentemente)
                if (entity != null && lockOnSelectedEntity == false)
                    method(entity);
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }


        internal static void OnMenuOpen(object sender, RoutedEventArgs e)
        {
            try
            {
                // recupera e verifica menu
                ContextMenu menu = sender as ContextMenu;

                Debug.Assert(menu != null);
                if (menu == null)
                    throw new ArgumentNullException("menu");

                // recupera e verifica entità
                IDCEntity entity = menu.Tag as IDCEntity;

                //Debug.Assert(entity != null);
                if (entity == null || entity.IsSystemEntity) // non voglio che le entità di sistema processano il contesto.
                {
                    menu.IsOpen = false;
                    return;
                }

                // verifica gli items speciali
                foreach (MenuItem item in menu.Items)
                {
                    if (item.Name.Contains(DCDefinitions.CONTEXT_REMOVE))
                    {
                        item.IsEnabled = entity.Deletable;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }

        internal static void OnMenuItemClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                // recupera e verifica l'item
                DCMenuItem item = sender as DCMenuItem;

                Debug.Assert(item != null);
                if (item == null)
                    throw new ArgumentNullException("item");

                // recupera entità(gia stata validata durante l'open)
                IDCEntity entity = item._menu.Tag as IDCEntity;

                // recupera canvas associato all'entità
                DCCanvas canvas = entity.GetCanvas();

                Debug.Assert(canvas != null);
                if (canvas == null)
                    throw new ArgumentNullException("canvas");

                // chiamo delegato(gia validato nel costr. di DCMenuItem)
                ExecuteContestualCommand(canvas, item._command, entity, item._orderSelection, item._selectionAsc);
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }

    }
}
