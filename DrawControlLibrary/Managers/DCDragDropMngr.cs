﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Windows.Controls;
using DrawControlLibrary.Entities;
using System.Windows.Media.Effects;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe manager per la gestione del drag-drop di elementi, contiene eventi e procedure
    /// </summary>
    internal static class DCDragDropMngr
    {
        /// <summary>
        /// Esegue un azione visiva quando viene iniziata un operazione di drag
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="e"></param>
        internal static void OnDragEnter(DCCanvas canvas, DragEventArgs e)
        {
        }

        /// <summary>
        /// Esegue un azione visiva quando viene "annullata" un operazione di drag
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="e"></param>
        internal static void OnDragLeave(DCCanvas canvas, DragEventArgs e)
        {
        }

        /// <summary>
        /// Esegue un azione visiva in funzione al tipo di oggetto che si sta draggando
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="e"></param>
        /// <param name="objData"></param>
        /// <param name="format"></param>
        internal static void OnDragOver(DCCanvas canvas, DragEventArgs e, object objData, string format)
        {
            // in base a quello che sto draggando la grafica assumerà un risultato

            switch (format)
            {
                case DCDefinitions.DRAG_FORMAT_EFFECT:
                    canvas.SetPrompt("Trascina l'effetto su un entità");
                    break;

                case DCDefinitions.DRAG_FORMAT_CLIP:
                    canvas.SetPrompt("Rilascia nel punto in cui vuoi inserire l'immagine");
                    break;

                default:
                    canvas.SetPrompt("TODO");
                    break;
            }
        }

        /// <summary>
        /// Esegue un azione logica in funzione al tipo di oggetto che si è draggato
        /// </summary>
        /// <param name="canvas"></param>
        /// <param name="e"></param>
        /// <param name="objData"></param>
        /// <param name="format"></param>
        internal static void OnDrop(DCCanvas canvas, DragEventArgs e, object objData, string format)
        {
            // applica il dato

            switch (format)
            {
                case DCDefinitions.DRAG_FORMAT_EFFECT:
                    IDCEntity entity = _tryGetTouchedEntity(canvas, e);
                    if(entity!=null)
                        entity.SetEffect(objData as Effect);
                    break;
                    
                case DCDefinitions.DRAG_FORMAT_CLIP:
                    dynamic clip = objData;
                    ImageSource bitmap = clip.ImageClip;

                    DCImage image = new DCImage();
                        image.Position = e.GetPosition(canvas);
                        image.Width = bitmap.Width;
                        image.Height = bitmap.Height;
                        image.Src = bitmap;

                    canvas.AddEntity(image);
                    break;

                default:
                    break;
            }
        }




        // valida gli oggetti di routine, se fallisce genera un eccezione
        private static void _validateFirst(DCCanvas canvas, DragEventArgs e)
        {
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");
            Debug.Assert(e != null);
            if (e == null)
                throw new ArgumentNullException("e");
        }

        // ottiene l'entità toccata dal mouse
        private static IDCEntity _tryGetTouchedEntity(DCCanvas canvas, System.Windows.DragEventArgs e)
        {
            // l'effetto non ha bisogno dell'assert
            _validateFirst(canvas, e);

            // ottiene hit alla posizione del mouse attuale
            HitTestResult hitTestResult = VisualTreeHelper.HitTest(canvas, e.GetPosition(canvas));

            // si tratta di un entità?
            if (hitTestResult.VisualHit != null && hitTestResult.VisualHit is IDCEntity)
            {
                // recupera l'entità toccata
                IDCEntity entity = hitTestResult.VisualHit as IDCEntity;

                return entity;
            }
            else // prova a ricavarla
            {
                // trova l'entità associata
                IDCEntity entity = DCUtils.FindIDCEntityParent(hitTestResult.VisualHit);

                return entity;
            }
        }
    }
}
