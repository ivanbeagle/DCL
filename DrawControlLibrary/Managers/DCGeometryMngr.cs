﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe manager per la generazione di geometrie utilizzate da oggetti DCShape
    /// </summary>
    internal static class DCGeometryMngr
    {
        /// <summary>
        /// Verifica la forma e crea il tipo di geometria specificato nello ShapeType
        /// </summary>
        /// <param name="shape"></param>
        /// <returns></returns>
        internal static bool GetGeometry(DCShape shape)
        {
            Debug.Assert(shape != null);
            if (shape == null)
                return false;

            // verifica che ci siano almeno 2 punti
            if (shape.Points.Count < 2)
                return false;

            try
            {
                switch (shape.ShapeType)
                {
                    case DCShapeType.Line:
                        _line(shape);
                        break;

                    case DCShapeType.Rectangle:
                        _rect(shape);
                        break;

                    case DCShapeType.Arrow:
                        _arrow(shape);
                        break;

                    case DCShapeType.Ellipse:
                        _ellipse(shape);
                        break;

                    case DCShapeType.Polygon:
                        _polyline(shape,true);
                        break;

                    case DCShapeType.Polyline:
                        _polyline(shape,false);
                        break;

                    case DCShapeType.Bezier:
                        _bezier(shape);
                        break;

                    case DCShapeType.CurvedPolygon:
                        _curvedPolygon(shape);
                        break;

                    case DCShapeType.CloudPolygon:
                        _comicCloud(shape);
                        break;

                    default:
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        #region Metodi di geometrie e shapes disponibili
        private static void _line(DCShape shape)
        {
            shape._geometry = new LineGeometry(shape.Points[0], shape.Points[1]);
        }

        private static void _rect(DCShape shape)
        {
            double smooth = shape.Smooth;
            shape._geometry = new RectangleGeometry(new Rect(shape.Points[0], shape.Points[1]), smooth, smooth);
        }

        private static void _ellipse(DCShape shape)
        {
            shape._geometry = new EllipseGeometry(shape.Points[0], shape.Points[1].X, shape.Points[1].Y);
        }

        private static void _arrow(DCShape shape)
        {
            // bisogno di almeno 3 punti, altrimenti crea una linea
            if (shape.Points.Count < 3)
            {
                shape._geometry = new LineGeometry(shape.Points[0], shape.Points[1]);
                return;
            }

            // recupera coordinate della linea
            double X1, Y1, X2, Y2;
            X1 = shape.Points[0].X;
            X2 = shape.Points[1].X;
            Y1 = shape.Points[0].Y;
            Y2 = shape.Points[1].Y;

            // altezza e larghezza della freccia
            double headHeight = shape.Points[2].Y;
            double headWidth = shape.Points[2].X;

            // calcolo
            double theta = Math.Atan2(Y1 - Y2, X1 - X2);
            double sint = Math.Sin(theta);
            double cost = Math.Cos(theta);
            Point pt1 = new Point(X1, Y1);
            Point pt2 = new Point(X2, Y2);
            Point pt3 = new Point(X2 + (headWidth * cost - headHeight * sint), Y2 + (headWidth * sint + headHeight * cost));
            Point pt4 = new Point(X2 + (headWidth * cost + headHeight * sint), Y2 - (headHeight * cost - headWidth * sint));

            // crea la forma
            GeometryGroup groupGeometry = new GeometryGroup();
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext streamGeometryContext = streamGeometry.Open())
            {
                streamGeometryContext.BeginFigure(pt1, false, false);
                streamGeometryContext.LineTo(pt2, true, true);
            }
            StreamGeometry streamGeometry2 = new StreamGeometry();
            using (StreamGeometryContext streamGeometryContext2 = streamGeometry2.Open())
            {
                streamGeometryContext2.BeginFigure(pt3, true, true);
                streamGeometryContext2.LineTo(pt2, true, true);
                streamGeometryContext2.LineTo(pt4, true, true);
                streamGeometryContext2.LineTo(pt3, true, true);
            }

            groupGeometry.Children.Add(streamGeometry);
            groupGeometry.Children.Add(streamGeometry2);
            
            shape._geometry = groupGeometry;
        }

        private static void _polyline(DCShape shape, bool isClosed)
        {
            StreamGeometry streamGeometry = new StreamGeometry();
            streamGeometry.FillRule = FillRule.EvenOdd;

            using (StreamGeometryContext streamGeometryContext = streamGeometry.Open())
            {
                streamGeometryContext.BeginFigure(shape.Points[0], true, isClosed);
                foreach (Point point in shape.Points)
                    streamGeometryContext.LineTo(point, true, true);
            }

            shape._geometry = streamGeometry;
        }

        private static void _bezier(DCShape shape)
        {
            PathFigure pathFigure = new PathFigure();
            PolyBezierSegment bezierSegment = new PolyBezierSegment();
            PathSegmentCollection pathSegmentCollection = new PathSegmentCollection();
            PathFigureCollection pathFigureCollection = new PathFigureCollection();
            PathGeometry pathGeometry = new PathGeometry();

            pathFigure.StartPoint = shape.Points[0];
            bezierSegment.Points = shape.Points;
            pathSegmentCollection.Add(bezierSegment);
            pathFigure.Segments = pathSegmentCollection;
            pathFigureCollection.Add(pathFigure);
            pathGeometry.Figures = pathFigureCollection;

            shape._geometry = pathGeometry;
        }

        private static void _curvedPolygon(DCShape shape)
        {
            double smooth = shape.Smooth;
            int pointCount = shape.Points.Count;

            PathFigure pathFigure = new PathFigure();
            PathFigureCollection pathFigureCollection = new PathFigureCollection();
            PathGeometry pathGeometry = new PathGeometry();

            pathFigure.StartPoint = shape.Points[(0 + 1) % pointCount];
            PathSegmentCollection pathSegmentCollection = new PathSegmentCollection();

            for (int i = 0; i < pointCount; i++)
            {
                var point0 = shape.Points[(i + 0) % pointCount];
                var point1 = shape.Points[(i + 1) % pointCount];
                var point2 = shape.Points[(i + 2) % pointCount];
                var point3 = shape.Points[(i + 3) % pointCount];
                pathSegmentCollection.Add(DCSupport.PolygonSegment((Vector)point0, (Vector)point1, (Vector)point2, (Vector)point3, smooth));
            }

            pathFigure.Segments = pathSegmentCollection;
            pathFigureCollection.Add(pathFigure);
            pathGeometry.Figures = pathFigureCollection;
            
            shape._geometry = pathGeometry;
        }

        private static void _comicCloud(DCShape shape, bool isLargeArc=true, SweepDirection clockWiseDirection = SweepDirection.Clockwise)
        {
            double param1 = shape.Parameter1;
            double param2 = shape.Parameter2;
            double smooth = shape.Smooth;

            StreamGeometry streamGeometry = new StreamGeometry();
            streamGeometry.FillRule = FillRule.EvenOdd;

            using (StreamGeometryContext streamGeometryContext = streamGeometry.Open())
            {
                streamGeometryContext.BeginFigure(shape.Points[0], true, true);
                foreach (Point point in shape.Points)
                    streamGeometryContext.ArcTo(point, new Size(Math.Abs(param1), Math.Abs(param2)), smooth, isLargeArc, clockWiseDirection, true, true);
            }

            shape._geometry = streamGeometry;
        }
        #endregion
    }
}
