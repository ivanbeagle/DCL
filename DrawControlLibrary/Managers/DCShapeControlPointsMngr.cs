﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary.Managers
{
    /// <summary>
    /// Classe manager per la gestione dei punti di controllo di un entità shape
    /// </summary>
    public class DCShapeControlPointsMngr : DCAbstractControlPointMngr
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="entity"></param>
        public DCShapeControlPointsMngr(IDCEntity entity) : base(entity) { }

        // aggiunge i punti di controllo al canvas
        public override void AddPoints()
        {
            base.AddPoints();

            // recupera shape
            DCShape shape = _entity as DCShape;

            // recupera canvas
            DCCanvas canvas = _entity.GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // recupera la posizione
            Point position  = _entity.Position;

            int indexPoint = 0;

                if (shape.ShapeType == DCShapeType.Ellipse)  // caso di eccezione per il cerchio(faccio aggiungere solo un punto anche se ne ha 2)
                {
                    // crea il punto
                    DCControlPoint cp = new DCControlPoint(_entity, 1);
                        cp.Cursor = Cursors.SizeAll;
                        DCCanvas.SetLeft(cp, position.X + shape.Points[1].X);
                        DCCanvas.SetTop(cp, position.Y + shape.Points[1].Y);
                    
                    canvas.Children.Add(cp);
                }
                else // caso shape generici
                {
                    foreach (Point p in shape.Points)
                    {
                        DCControlPoint cp = new DCControlPoint(_entity, indexPoint); 
                            cp.Cursor = Cursors.SizeAll;
                            DCCanvas.SetLeft(cp, position.X + p.X);
                            DCCanvas.SetTop(cp, position.Y + p.Y);
                        
                        canvas.Children.Add(cp);

                        indexPoint++;
                    }
                }
        }

        // si verifica quando cambia la posizione
        public override void Update()
        {
            // recupera shape
            DCShape shape = _entity as DCShape;

            // recupera canvas
            DCCanvas canvas = _entity.GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // recupera la posizione
            Point position = _entity.Position;

            // aggiorna i punti che appartengono allo shape
            foreach (DCControlPoint cp in canvas._cacheControlPoints)
            {
                if (cp._entity == _entity)
                {
                    DCCanvas.SetLeft(cp, position.X + shape.Points[cp._index].X);
                    DCCanvas.SetTop(cp, position.Y + shape.Points[cp._index].Y);
                }
            }
        }
    }
}
