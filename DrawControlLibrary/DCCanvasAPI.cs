﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DrawControlLibrary.Managers;
using iTextSharp.text;

namespace DrawControlLibrary
{
    /// <summary>
    /// API DCCanvas
    /// </summary>
    public partial class DCCanvas : System.Windows.Controls.Canvas, INotifyPropertyChanged
    {
        /// <summary>
        /// Calcola la misura raw nella misura di riferimento
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetMeasure(double value)
        {
            return string.Format("{0:0.00} {1}", (value / MeasureReference), MeasureReferenceCaption); 
        }

        /// <summary>
        /// Setta il formato del foglio
        /// </summary>
        /// <param name="df"></param>
        public void SetSheetFormat(DCSheetFormat df)
        {
            Debug.Assert(df!=null);
            if (df == null)
                return;

            // assegna le dimensioni
            _sheetBoxWidth = DCUtils.GetPxByCm(df.Width);
            _sheetBoxHeight = DCUtils.GetPxByCm(df.Height);

            // muove il punto di controllo associato
            UpdateSheetBox();

            // rigenera il canvas e i suoi elementi
            Regen(true);

            // lancia evento
            if (SheetSizeChanged != null)
                SheetSizeChanged(this, new DCSheetEventArgs(SheetWidth, SheetHeight));
        }

        /// <summary>
        /// Ridisegna immediatamente tutte le entità
        /// </summary>
        /// <param name="canvas"></param>
        public void RefreshEntities()
        {
            foreach (FrameworkElement element in Children)
            {
                if (element is IDCEntity)
                {
                    element.InvalidateVisual();
                    element.InvalidateMeasure();
                }
            }
        }

        /// <summary>
        /// Invalida l'area di disegno e tutte le entità
        /// </summary>
        public void Regen(bool regenLayout = false)
        {
            // invalida la grafica
            InvalidateVisual();

            // aggiorna entità
            RefreshEntities();

            // rigenera il layout
            if (regenLayout)
            {
                InvalidateMeasure();
                UpdateLayout();
            }
        }

        /// <summary>
        /// Setta lo sfondo per il foglio
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool SetBackground(string fileName)
        {
            // validazione
            bool isValidFile = DCUtils.CheckFile(fileName);
            Debug.Assert(isValidFile);
            if (!isValidFile)
                return false;

            try
            {
                _background.Source = new BitmapImage(new Uri(fileName, UriKind.RelativeOrAbsolute));
                EnableBackground(true);
                return true;
            }
            catch(Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
                return false;
            }
        }

        /// <summary>
        /// Esporta il disegno in PDF
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="includeZoom"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public bool ExportPdf(string fileName, bool includeZoom=false, int width=0, int height=0)
        {
            try
            {
                // validazione
                bool isValidFile = !String.IsNullOrWhiteSpace(fileName);
                Debug.Assert(isValidFile);
                if (!isValidFile)
                    return false;

                // esporta l'immagine del canvas
                string tempName = Path.GetTempFileName();
                if (!ExportImage(tempName, includeZoom, width, height))
                    return false;

                // itext

                // crea documento
                Document document = new Document();

                // associa lo stream
                FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, stream);

                // apre il documento
                document.Open();

                // elabora l'immagine
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(new Uri(tempName));

                image.SetDpi((int)DCUtils.DPIX, (int)DCUtils.DPIY);

                // recupera dimensioni
                float imageWidth = image.Width;
                float imageHeight = image.Height;
                
                // setta i margini
                document.SetMargins(0, 0, 0, 0);

                // setta dimensione della prossima pagina
                document.SetPageSize(new iTextSharp.text.Rectangle(imageWidth, imageHeight));

                // aggiunge una pagina
                document.NewPage();

                // aggiunge l'immagine
                document.Add(image);

                // chiude il flusso
                document.Close();

                return true;
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
                return false;
            }
        }

        /// <summary>
        /// Ottiene l'immagine raster del canvas
        /// </summary>
        /// <returns></returns>
        public BitmapSource GetRaster()
        {
            // recupera la transform
            Transform transform = LayoutTransform;

            // annulla la transform corrente, sarà ripristinata poi
            LayoutTransform = null;

            // elabora la dimensione reale del canvas
            Size size = new Size(ActualWidth, ActualHeight);

            // esegue una misurazione e un arrange del canvas, non so a che serve ma è importante!
            Measure(size);
            Arrange(new Rect(size));

            // renderizza
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)SheetWidth, (int)SheetHeight, DCUtils.DPIX, DCUtils.DPIY, PixelFormats.Pbgra32);
            rtb.Render(this);

            // ripristina la transform
            LayoutTransform = transform;

            return rtb;
        }

        /// <summary>
        /// Ottiene un'area del raster delimitata in un rettangolo(cropping) a patto che lo zoom attuale sia impostato su 1
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="includeZoom"></param>
        /// <returns></returns>
        public BitmapSource GetCroppedRasterRaw(int x, int y, int width, int height)
        {
            // non esegue se lo zoom è stato variato
            if (Zoom != 1)
                return null;

            // recupera raster
            BitmapSource raster = GetRaster();
            Debug.Assert(raster != null);
            if (raster == null)
                return null;

            try
            {
                // crop
                CroppedBitmap crop = new CroppedBitmap(raster, new Int32Rect
                (
                x,          // (int)DCUtils.CalcScale(x, scale.ScaleX),
                y,          // (int)DCUtils.CalcScale(y, scale.ScaleY),
                width,      // (int)DCUtils.CalcScale(width, scale.ScaleX),
                height      // (int)DCUtils.CalcScale(height, scale.ScaleY)
                ));

                return crop;
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
                return null;
            }
        }

        /// <summary>
        /// Esporta il disegno come file immagine
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="includeZoom"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public bool ExportImage(string fileName, bool includeZoom=false, int width=0, int height=0)
        {
            // validazione
            bool isValidFile = !String.IsNullOrWhiteSpace(fileName);
            Debug.Assert(isValidFile);
            if (!isValidFile)
                return false;

            // crea raster
            //BitmapSource raster = GetRaster(width, height, includeZoom);
            BitmapSource raster = GetRaster();

            return DCUtils.SaveImage(raster, fileName);
        }

        public void NotifyCoordinatesTo(Label coordinatesLbl)
        {
            _coordinatesLbl = coordinatesLbl;
        }

        public void NotifySheetSizeTo(Label sheetSizeLbl)
        {
            _sheetSizeLbl = sheetSizeLbl;
        }

        /// <summary>
        /// Rimuove l'entità dal disegno
        /// </summary>
        /// <param name="entity"></param>
        public void RemoveEntity(IDCEntity entity)
        {
            _removeEntity(entity, true, false);
        }

        /// <summary>
        /// Aggiunge l'entità al disegno
        /// Nota: (NON utilizzare il Children.Add)
        /// </summary>
        /// <param name="entity"></param>
        public void AddEntity(IDCEntity entity)
        {
            AddEntity(entity, true);
        }

        /// <summary>
        /// Seleziona tutte le entità "selezionabili" nel disegno
        /// </summary>
        public void SelectAll()
        {
            List<IDCEntity> listOfSelection = new List<IDCEntity>();
            foreach (FrameworkElement element in Children)
            {
                if (element is IDCEntity)
                {
                    IDCEntity entity = element as IDCEntity;

                    // aggiungo se è selezionabile
                    if (entity.Selectable)
                        listOfSelection.Add(element as IDCEntity);
                }
            }

            // contrassegna come selezionato
            foreach (IDCEntity entity in listOfSelection)
                if (!entity.Selected)
                    entity.Selected = true;
        }

        /// <summary>
        /// Deseleziona tutte le entità selezionate
        /// </summary>
        public void UnSelectAll()
        {
            List<IDCEntity> listOfDeselection = new List<IDCEntity>();
            foreach (IDCEntity entity in Selection)
                listOfDeselection.Add(entity);

            foreach (IDCEntity entity in listOfDeselection)
                if (entity.Selected)
                    entity.Selected = false;

            GC.Collect();

        }

        /// <summary>
        /// Rimuove dal disegno le entità selezionate
        /// </summary>
        public void EraseSelection()
        {
            List<IDCEntity> listOfErase = new List<IDCEntity>();
            foreach (IDCEntity entity in Selection)
                listOfErase.Add(entity);

            foreach (IDCEntity entity in listOfErase)
                RemoveEntity(entity);

            GC.Collect();

        }

        /// <summary>
        /// Setta un "tool" al canvas, il tool agisce in funzione dell'input ricevuto sul canvas
        /// </summary>
        /// <param name="tool"></param>
        /// <param name="param"></param>
        public void SetTool(DCAbstractTool tool, object param=null)
        {
            // puo essere null
            if (_lastTool != null)
            {
                // verifica lo stato di abort
                if (_lastTool.IsAbort)
                    _lastTool.OnAbort();

                // chiama sempre finish
                _lastTool.Finish();
            }

            // setta il nuovo tool
            _lastTool = tool;

            // notifica
            FirePropertyChanged("HasToolActived");

            if (_lastTool != null)
            {
                // associo il canvas al tool
                _lastTool._canvas = this;

                // notifica start
                _lastTool.Start(param);
            }
        }

        /// <summary>
        /// Porta in primo piano l'entità
        /// </summary>
        /// <param name="entity"></param>
        public void BringToFront(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // refresh entità
            InternalChildren.Remove(element);
            InternalChildren.Add(element);

            // refresh dei punti di controllo
            if (entity.ControlPointsManager != null)
                entity.ControlPointsManager.Regen();
        }

        /// <summary>
        /// Porta l'entità dietro a tutto
        /// </summary>
        /// <param name="entity"></param>
        public void SendToBack(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // refresh entità
            InternalChildren.Remove(element);
            InternalChildren.Insert(DCDefinitions.FIRST_INDEX, element);    // inizia da ???

            // refresh dei punti di controllo
            if (entity.ControlPointsManager != null)
                entity.ControlPointsManager.Regen();
        }

        /// <summary>
        /// Sposta l'entità indietro
        /// </summary>
        /// <param name="entity"></param>
        public void MoveBack(IDCEntity entity)
        {
            // valida entita
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // valida indice
            int? index = entity.GetZIndex();
            if (index == null)
                return;
            if (index <= DCDefinitions.FIRST_INDEX + 1)
                return;

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // manda dietro
            InternalChildren.Remove(element);
            InternalChildren.Insert(index.Value - 1, element);

            // refresh dei punti di controllo
            if (entity.ControlPointsManager != null)
                entity.ControlPointsManager.Regen();
        }

        /// <summary>
        /// Sposta l'entità avanti
        /// </summary>
        /// <param name="entity"></param>
        public void MoveNext(IDCEntity entity)
        {
            // almeno 2 entità per questo metodo!
            if (GetEntities().Count() < 2)
                return;

            // valida entita
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // valida indice
            int? index = entity.GetZIndex();
            if (index == null)
                return;

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (element == null)
                throw new ArgumentNullException("element");

            // manda avanti
            InternalChildren.Remove(element);
            InternalChildren.Insert(index.Value + 1, element);

            // refresh dei punti di controllo
            if (entity.ControlPointsManager != null)
                entity.ControlPointsManager.Regen();
        }

        /// <summary>
        /// Ottiene la lista di entità nel disegno
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IDCEntity> GetEntities()
        {
            return InternalChildren.OfType<IDCEntity>().Where(entity => entity.IsSystemEntity == false);
        }

        /// <summary>
        /// Ottiene l'entità da id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<IDCEntity> GetEntitiesById(string id)
        {
            if (String.IsNullOrEmpty(id))
                return GetEntities();

            return InternalChildren.OfType<IDCEntity>().Where(entity => entity.IsSystemEntity == false && entity.Id == id);
        }

        /// <summary>
        /// Pulisce il disegno
        /// </summary>
        public void ClearDocument()
        {
            // cosi facendo posso utilizzare il foreach
            IDCEntity[] entities = GetEntities().ToArray();

            foreach (IDCEntity entity in entities)
                _removeEntity(entity, false, true);

            // disabilita il background
            EnableBackground(false);
        }

        /// <summary>
        /// Salva il disegno nel file specificato in formato xml
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="saveBackground"></param>
        public void SaveDocument(String fileName, bool saveBackground=true)
        {
            try
            {
                // validazione
                bool isValidFile = !String.IsNullOrWhiteSpace(fileName);
                Debug.Assert(isValidFile);
                if (!isValidFile)
                    throw new ArgumentNullException("fileName");

                // crea la condizione d'uso per il background
                bool useBackground = saveBackground == true && _background.Source != null;

                // elabora stream
                List<string> stream = new List<string>();
                stream.Add("<!-- Generated by DrawControlLibrary; do not edit!!! -->");
                stream.Add(string.Format("<DCDocument version='{0}' sheetWidth='{1}' sheetHeight='{2}' sheetColor='{3}' background='{4}' enableMeasureReference='{5}' measureReferenceCaption='{6}' measureReference='{7}'>", 
                    Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    Convert.ToInt32(SheetWidth, DCDefinitions.NumberFormat),
                    Convert.ToInt32(SheetHeight, DCDefinitions.NumberFormat), 
                    DCSupport.GetBrushStr(SheetColor),
                    useBackground==false ? string.Empty : ((BitmapImage)_background.Source).UriSource.ToString().Replace("file:///",""),
                    EnableMeasureReference,
                    MeasureReferenceCaption,
                    MeasureReference.ToString(DCDefinitions.NumberFormat)
                    ));

                // ottiene serializzazione oggetti
                foreach (IDCEntity entity in GetEntities())
                    stream.Add(entity._getSerialization(fileName));

                // chiude documento
                stream.Add(string.Format("</DCDocument>"));

                // scrive
                File.WriteAllLines(fileName, stream);
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }

        /// <summary>
        /// Carica il documento xml come nuovo disegno
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool LoadDocument(string fileName)
        {
            // validazione
            bool isValidFile = DCUtils.CheckFile(fileName);
            Debug.Assert(isValidFile);
            if (!isValidFile)
                return false;

            // a differenza dell' import il load cancella la vecchia elaborazione
            ClearDocument();

            return ImportDocument(fileName,true);
        }

        /// <summary>
        /// Importa il documento xml nel disegno attuale
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="loadBackground"></param>
        /// <returns></returns>
        public bool ImportDocument(String fileName, bool loadBackground=false)
        {
            try
            {
                DCStreamMngr importMngr = new DCStreamMngr(this);
                return importMngr.ImportFile(fileName, loadBackground);
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
                return false;
            }
        }

        /// <summary>
        /// Abilita l'interazione con il foglio
        /// </summary>
        /// <param name="value"></param>
        public void EnableSheetInteraction(bool value)
        {
            _sheetBoxControl.Visibility = value ? Visibility.Visible : Visibility.Hidden;
        }

        /// <summary>
        /// Abilita la selezione rettangolare(multipla) 
        /// </summary>
        /// <param name="value"></param>
        public void EnableRectangularSelection(bool value)
        {
            _enableSelectionBox = value;
        }

        /// <summary>
        /// Abilita lo sfondo
        /// </summary>
        /// <param name="value"></param>
        public void EnableBackground(bool value)
        {
            // ho caricato uno sfondo?
            if (_background.Source == null)
                return;

            if (!value)
            {
                _background.Visibility = System.Windows.Visibility.Hidden;

                // riabilita il punto di controllo per il ridimensionamento del foglio
                _sheetBoxControl.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                _background.Visibility = System.Windows.Visibility.Visible;
                SheetWidth = _background.Source.Width;
                SheetHeight = _background.Source.Height;

                // disabilita il punto di controllo per il ridimensionamento del foglio
                _sheetBoxControl.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        /// <summary>
        /// Ottiene la selezione ordinata di entità nel disegno
        /// </summary>
        /// <param name="ascending"></param>
        /// <returns></returns>
        public IEnumerable<IDCEntity> GetOrderSelection(bool ascending = true)
        {
            try
            {
                if (ascending)
                    return Selection.OrderBy(entity => entity.GetZIndex());

                return Selection.OrderByDescending(entity => entity.GetZIndex());
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
                return Selection;
            }
        }

        /// <summary>
        /// Esegue il refresh del prompt
        /// </summary>
        public void RefreshPrompt()
        {
            InternalChildren.Remove(Prompt);
            InternalChildren.Add(Prompt);
        }

        /// <summary>
        /// Setta il testo del prompt
        /// </summary>
        /// <param name="text"></param>
        public void SetPrompt(string text)
        {
            if (text == null)
                text = String.Empty;

            Prompt.Content = text;
        }
    }
}
