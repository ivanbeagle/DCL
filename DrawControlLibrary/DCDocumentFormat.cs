﻿namespace DrawControlLibrary
{
    /// <summary>
    /// Contiene formati foglio(in Cm)
    /// </summary>
    public class DCSheetFormat
    {
        /// <summary>
        /// W Cm
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// H Cm
        /// </summary>
        public double Height { get; set; }

        public DCSheetFormat(double wCm, double hCm)
        {
            Width=wCm;
            Height=hCm;
        }

        public static readonly DCSheetFormat A10 = new DCSheetFormat(2.6, 3.7);
        public static readonly DCSheetFormat A9 = new DCSheetFormat(3.7, 5.2);
        public static readonly DCSheetFormat A8 = new DCSheetFormat(5.2, 7.4);
        public static readonly DCSheetFormat A7 = new DCSheetFormat(7.4, 10.5);
        public static readonly DCSheetFormat A6 = new DCSheetFormat(10.5, 14.8);
        public static readonly DCSheetFormat A5 = new DCSheetFormat(14.8, 21);
        public static readonly DCSheetFormat A4 = new DCSheetFormat(21, 29.7);
        public static readonly DCSheetFormat A3 = new DCSheetFormat(29.7, 42);
        public static readonly DCSheetFormat A2 = new DCSheetFormat(42, 59.4);
        public static readonly DCSheetFormat A1 = new DCSheetFormat(54.9, 84.1);
        public static readonly DCSheetFormat A0 = new DCSheetFormat(84.1, 118.9);
    }
}
