﻿using System;
using System.Diagnostics;
using System.Windows;
using DrawControlLibrary.Entities;
using System.Windows.Media;

namespace DrawControlLibrary
{
    /// <summary>
    /// Classe di supporto minimale per lo sviluppo di Tools
    /// </summary>
    public static class DCToolHelper
    {
        /// <summary>
        /// Esegue il setup di base per utilizzare uno Shape
        /// </summary>
        /// <param name="shape"></param>
        public static void SetupShape(DCShape shape)
        {
            Debug.Assert(shape != null);
            if (shape == null)
                throw new ArgumentNullException("shape");

            if (!shape.IsSystemEntity)
                return;

            // setup
            shape.Fill = Brushes.White;
            shape.Stroke = Brushes.Black;
            shape.Visibility = Visibility.Visible;
            shape.Position = new Point(0, 0);
            shape.Points.Clear();

            // un minimo di 2 punti se no il measureoverride va in eccezione, il punto 0 corrisponde con la posizione
            shape.Points.Add(new Point(0, 0));
            shape.Points.Add(new Point(0, 0));
        }
    }
}
