﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using DrawControlLibrary.Entities;

namespace DrawControlLibrary
{
    /// <summary>
    /// Fornisce metodi di estensione sulle entità
    /// </summary>
    public static class DCEntityExt
    {
        public static Point GetRelativeCenter(this IDCEntity entity)
        {
            var bounds = entity.GetBounds();
            return new Point(bounds.Width / 2, bounds.Height / 2);
        }

        public static bool IsCaptured(this IDCEntity entity)
        {
            return (bool)entity.Data[DCDefinitions.KEY_CAPTURED];
        }

        public static bool IsDragging(this IDCEntity entity)
        {
            return (bool)entity.Data[DCDefinitions.KEY_DRAGGING];
        }

        public static bool IsMouseOver(this IDCEntity entity)
        {
            return (bool)entity.Data[DCDefinitions.KEY_MOUSE_OVER];
        }

        /// <summary>
        /// Rigenera
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="includePoints"></param>
        /// <param name="regenPoints"></param>
        public static void UpdateAll(this IDCEntity entity, bool includePoints=true, bool regenPoints=false)
        {
            FrameworkElement element = entity as FrameworkElement;
            Debug.Assert(element != null);
            if (element == null)
                return;

            element.InvalidateMeasure();
            //element.UpdateLayout();
            element.InvalidateVisual();

            if (includePoints)
            {
                if (entity.ControlPointsManager != null)
                {
                    if (!regenPoints)
                        entity.ControlPointsManager.Update();
                    else
                        entity.ControlPointsManager.Regen();
                }
            }
        }

        /// <summary>
        /// Setta come entità catturata(corrente)
        /// </summary>
        /// <param name="entity"></param>
        public static void SetAsCaptured(this IDCEntity entity)
        {
            DCCanvas canvas = entity.GetCanvas();
            Debug.Assert(canvas != null);
            if (canvas == null)
                return;

            entity.Data[DCDefinitions.KEY_CAPTURED] = true;

            canvas._lastCapturedEntity = entity;
        }

        /// <summary>
        /// Ottiene indice di visualizzazione
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static int? GetZIndex(this IDCEntity entity)
        {
            // recupera canvas
            DCCanvas canvas = entity.GetCanvas();
            // non è necessario un assert
            if (canvas == null)
                return null;

            // cicla i figli e ottiene indice
            for(int index=0; index < canvas.Children.Count; index++)
            {
                if (canvas.Children[index] == entity)
                    return index;
            }

            return null;
        }

        /// <summary>
        /// Ottiene la dimensione "WPF" dell'entità
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static Size? GetActualSize(this IDCEntity entity)
        {
            FrameworkElement element = entity as FrameworkElement;
            Debug.Assert(element != null);
            if (element == null)
                return null;

            return new Size(element.ActualWidth, element.ActualHeight);
        }

        /// <summary>
        /// Ottiene l'immagine raster dell'entità come oggetto DCImage
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static DCImage GetRaster(this IDCEntity entity)
        {
            RenderTargetBitmap rtb = entity.RenderToBitmap();
            if(rtb==null)
                return null;

            // crea una nuova entità
            DCImage image = new DCImage();
            image.Src = rtb;
            image.Position = entity.Position.GetClone();

            return image;
        }

        /// <summary>
        /// Setta l'effetto grafico all'entità specificata
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="effect"></param>
        public static void SetEffect(this IDCEntity entity, Effect effect)
        {
            dynamic dynamicEntity = null;
            if (entity is DCShape)
                dynamicEntity = entity as DCShape;
            else // if(entity is DCHost)
            {
                dynamicEntity = entity as DCHost;
                dynamicEntity.Content.Effect = effect;
            }
            dynamicEntity.Effect = effect;
        }

        /// <summary>
        /// Verifica tipo entità
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool IsShape(this IDCEntity entity)
        {
            return entity is DCShape;
        }
    }
}
