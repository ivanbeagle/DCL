﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrawControlLibrary
{
    /// <summary>
    /// Logica di interazione per DCMeasureInputDlg.xaml
    /// </summary>
    public partial class DCMeasureInputDlg : Window
    {
        public double Measure { get; set; }

        public DCMeasureInputDlg()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void _undoBtn_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void _okBtn_Click_1(object sender, RoutedEventArgs e)
        {
            if (Measure == 0)
                DialogResult = false;
            else
                DialogResult = true;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            _measureTxt.Focus();
            _measureTxt.SelectAll();
        }
    }
}
