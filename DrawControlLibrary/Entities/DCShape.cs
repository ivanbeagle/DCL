﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using DrawControlLibrary.Managers;

namespace DrawControlLibrary.Entities
{
    /// <summary>
    /// Entità Shape
    /// </summary>
    public class DCShape : System.Windows.Shapes.Shape, IDCEntity, INotifyPropertyChanged, ICloneable
    {
        #region Proprietà Comuni

        /// <summary>
        /// Ottiene o imposta l'ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Ottiene o imposta il manager dei punti di controllo
        /// </summary>
        public DCAbstractControlPointMngr ControlPointsManager { get; set; }

        /// <summary>
        /// Ottiene o imposta se i punti di controllo sono editabili
        /// </summary>
        public bool Editable { get; set; }

        /// <summary>
        /// Ottiene o imposta se è possibile cancellare l'entità
        /// </summary>
        public bool Deletable { get; set; }

        /// <summary>
        /// Ottiene o imposta se l'entità può essere selezionata
        /// </summary>
        public bool Selectable { get; set; }

        /// <summary>
        /// Ottiene o imposta il flag se l'entità è di sistema
        /// </summary>
        public bool IsSystemEntity { get; set; }

        /// <summary>
        /// Ottiene o imposta i dati associati a questa entità
        /// </summary>
        public Dictionary<string, object> Data { get; set; }


        /// <summary>
        /// Ottiene o imposta la posizione
        /// </summary>
        [DisplayName("Coordinate")]
        public Point Position
        {
            get { return this.GetPosition().Value; }
            set
            {
                DCCore.OnPositionChanged(this, value);
                FirePropertyChanged("Position");
            }
        }


        /*
        /// <summary>
        /// Ottiene o imposta la rotazione
        /// </summary>
        [DisplayName("Rotazione")]
        public double Rotation
        {
            get { return _rotation; }
            set
            {
                OnRotate(value);
                FirePropertyChanged("Rotation");
            }
        }*/


        /// <summary>
        /// Ottiene o imposta la scala
        /// </summary>
        [DisplayName("Scala")]
        public Point Scale
        {
            get { return new Point(_scaleTransform.ScaleX, _scaleTransform.ScaleY); }
            set
            {
                if (value != null)
                {
                    _onScale(value.X, value.Y);
                    FirePropertyChanged("Scale");
                }
            }
        }


        /// <summary>
        /// Ottine o imposta la selezione
        /// </summary>
        [DisplayName("Selezionato")]
        public bool Selected
        {
            get { return (bool)GetValue(SelectedProperty); }
            set
            {
                if (Selectable == true)
                    SetValue(SelectedProperty, value);
            }
        }
        public static readonly DependencyProperty SelectedProperty = DependencyProperty.Register("Selected", typeof(bool), typeof(DCShape), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(DCCore.OnSelect)));


        /// <summary>
        /// Ottiene o imposta il tipo di movimento consentito in un evento di spostamento
        /// </summary>
        public DCMoveableType MoveableType
        {
            get { return (DCMoveableType)GetValue(MoveableTypeProperty); }
            set { SetValue(MoveableTypeProperty, value); }
        }
        public static readonly DependencyProperty MoveableTypeProperty = DependencyProperty.Register("MoveableType", typeof(DCMoveableType), typeof(DCShape), new UIPropertyMetadata(DCMoveableType.MoveableFree));


        // eventi registrabili
        
        
        /// <summary>
        /// Questo evento viene chiamato quando si verifica un drag di questa entità
        /// </summary>
        public event EventHandler MoveEvent;

        /// <summary>
        /// Questo evento viene chiamato quando si modifica un punto di controllo di questa entità
        /// </summary>
        public event EventHandler<DCEditEventArgs> EditEvent;

        /// <summary>
        /// Questo evento viene chiamato quando si seleziona l'entità
        /// </summary>
        public event EventHandler<DCSelectionEventArgs> SelectedEvent;

        /// <summary>
        /// Questo evento viene chiamato quando si deseleziona l'entità
        /// </summary>
        public event EventHandler<DCSelectionEventArgs> UnSelectedEvent;

        /// <summary>
        /// Questo evento viene chiamato quando viene disegnata l'entità
        /// </summary>
        public event RenederDelegate RenderEvent;

        #endregion


        /// <summary>
        /// Costruttore
        /// </summary>
        public DCShape()
        {
            // inizializza e gestisce la creazione generica per la nuova entità
            DCCore.OnCreate(this);

            // crea lista di punti
            Points = new PointCollection();

            // inizializza trasformazioni
            _transformGroup = new TransformGroup();
            _scaleTransform = new ScaleTransform(1, 1, 0, 0);
            _rotateTransform = new RotateTransform(0, 0, 0);

            _transformGroup.Children.Add(_scaleTransform);
            _transformGroup.Children.Add(_rotateTransform);

            this.RenderTransform = _transformGroup;
            this.RenderTransformOrigin = new Point(0.5, 0.5);
        }


        #region Implementazioni
        public virtual string _getSerialization(object param = null)
        {
            return string.Format
                (
                "<DCShape fill='{0}' stroke='{1}' strokeThickness='{2}' shapeType='{3}' position='{4}' points='{5}' scale='{6}' data='{7}' id='{8}' smooth='{9}' parameter1='{10}' parameter2='{11}' parameter3='{12}' effect='{13}' showMeasureEnabled='{14}' />",
                DCSupport.GetBrushStr(Fill),
                DCSupport.GetBrushStr(Stroke),
                StrokeThickness.ToString(DCDefinitions.NumberFormat), 
                (int)ShapeType,
                Position.ToString(DCDefinitions.NumberFormat),
                Points.ToString(DCDefinitions.NumberFormat),
                Scale.ToString(DCDefinitions.NumberFormat), 
                DCSupport.GetDataStr(Data),
                Id,
                Smooth.ToString(DCDefinitions.NumberFormat),
                Parameter1.ToString(DCDefinitions.NumberFormat),
                Parameter2.ToString(DCDefinitions.NumberFormat),
                Parameter3.ToString(DCDefinitions.NumberFormat),
                DCSupport.GetEffectStr(Effect),
                ShowMeasureEnabled
                );
        }

        public System.Drawing.RectangleF GetBounds()
        {
            // invalida il bound
            this.InvalidateMeasure();

            // recupera il bound della geometria
            Rect bounds = _geometry.GetRenderBounds(null);

            // calcolo posizione esatta
            double xPos = Position.X + bounds.Left;
            double yPos = Position.Y + bounds.Top;

            // ritorno bounding box
            return new System.Drawing.RectangleF(
                (float)xPos,
                (float)yPos,
                (float)bounds.Width,
                (float)bounds.Height
                );
        }

        public Point GetCenter()
        {
            // invalida il bound
            this.InvalidateMeasure();

            // recupera il bound della geometria(se la geometry esiste)
            if (_geometry == null)
                return DCUtils.ZeroPt();

            Rect bounds = _geometry.GetRenderBounds(null);

            return new Point(bounds.Width / 2, bounds.Height / 2);
        }

        public DCCanvas GetCanvas() { return Parent as DCCanvas; }




        // handling eventi
        public void _notifyMoveEvent()
        {
            if (MoveEvent != null)
                MoveEvent(this, null);
        }

        public void _notifyEditEvent(int pointIndex)
        {
            DCEditEventArgs e = new DCEditEventArgs(pointIndex);
            if (EditEvent != null)
                EditEvent(this, e);
        }

        public void _notifySelectedEvent(DCSelectionType type)
        {
            if (SelectedEvent != null)
                SelectedEvent(this, new DCSelectionEventArgs(type));
        }

        public void _notifyUnSelectedEvent(DCSelectionType type)
        {
            if (UnSelectedEvent != null)
                UnSelectedEvent(this, new DCSelectionEventArgs(type));
        }

        private void _onScale(double factorX, double factorY, Point? center = null)
        {
            double centerX = 0;
            double centerY = 0;

            if (center == null)
                center = GetCenter();

            centerX = center.Value.X;
            centerY = center.Value.Y;

            _scaleTransform.ScaleX = factorX;
            _scaleTransform.ScaleY = factorY;
            _scaleTransform.CenterX = centerX;
            _scaleTransform.CenterY = centerY;

            this.UpdateAll();
        }

        /*private void _onRotate(double deg, Point? center=null)
        {
            double centerX = 0;
            double centerY = 0;

            if (center == null)
                center = GetCenter();

            centerX = center.Value.X;
            centerY = center.Value.Y;

            double angle = deg - _rotation;

            RotateTransform rotateTransform = new RotateTransform(angle, centerX, centerY);
            for (int i = 0; i < Points.Count; i++)
            {
                // applica la trasformazione direttamente ai punti
                Point transformedPoint = new Point();
                transformedPoint = rotateTransform.Transform(Points[i]);
                Points[i] = transformedPoint;

                _rotation = deg;
            }

            this.UpdateAll();
        }*/
        #endregion


        #region Interfaccia INotify
        public event PropertyChangedEventHandler PropertyChanged;
        public void FirePropertyChanged(string propertyName)
        {
            try
            {
                Debug.Assert(!String.IsNullOrEmpty(propertyName));
                if (String.IsNullOrEmpty(propertyName))
                    throw new ArgumentNullException("propertyName");

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }
        #endregion


        #region Altro
        // trasformazioni
        private RotateTransform _rotateTransform;
        private ScaleTransform _scaleTransform;
        private TransformGroup _transformGroup;

        internal Geometry _geometry;

        /// <summary>
        /// Immagine utilizzata solo durante il render
        /// (non viene serializzata ne clonata)
        /// </summary>
        /*public ImageSource RenderImage
        {
            get { return (ImageSource)GetValue(RenderImageProperty); }
            set { SetValue(RenderImageProperty, value); }
        }
        public static readonly DependencyProperty RenderImageProperty =
            DependencyProperty.Register("RenderImage", typeof(ImageSource), typeof(DCShape), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));
        */

        /// <summary>
        /// Ottiene o imposta l'insieme dei punti che compongono la geometria
        /// </summary>
        public PointCollection Points
        {
            get { return (PointCollection)GetValue(PointsProperty); }
            set { SetValue(PointsProperty, value); }
        }
        public static readonly DependencyProperty PointsProperty = DependencyProperty.Register("Points", typeof(PointCollection), typeof(DCShape), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));


        /// <summary>
        /// Ottiene o imposta il tipo di forma
        /// </summary>
        public DCShapeType ShapeType
        {
            get { return (DCShapeType)GetValue(ShapeTypeProperty); }
            set { SetValue(ShapeTypeProperty, value); }
        }
        public static readonly DependencyProperty ShapeTypeProperty = DependencyProperty.Register("ShapeType", typeof(DCShapeType), typeof(DCShape), new FrameworkPropertyMetadata(DCShapeType.Line, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(DCCore.RefreshControlPoints)));


        /// <summary>
        /// Ottiene o imposta il valore di "arrotondamento" ove applicabile
        /// </summary>
        [DisplayName("Morbidezza")]
        public double Smooth
        {
            get { return (double)GetValue(SmoothProperty); }
            set { SetValue(SmoothProperty, value); }
        }
        public static readonly DependencyProperty SmoothProperty =
            DependencyProperty.Register("Smooth", typeof(double), typeof(DCShape), new UIPropertyMetadata(0.7, new PropertyChangedCallback(
                delegate(DependencyObject obj, DependencyPropertyChangedEventArgs e)
                {
                    DCShape shape = obj as DCShape;
                    if (shape != null)
                        shape.UpdateAll(false);
                }
                )));


        #region Parametri aggiuntivi

        // parametro 1
        [DisplayName("Parametro A")]
        public double Parameter1
        {
            get { return (double)GetValue(Parameter1Property); }
            set { SetValue(Parameter1Property, value); }
        }
        public static readonly DependencyProperty Parameter1Property =
            DependencyProperty.Register("Parameter1", typeof(double), typeof(DCShape), new UIPropertyMetadata(0.9, new PropertyChangedCallback(
                delegate(DependencyObject obj, DependencyPropertyChangedEventArgs e)
                {
                    DCShape shape = obj as DCShape;
                    if (shape != null)
                        shape.UpdateAll(false);
                }
                )));


        // parametro 2
        [DisplayName("Parametro B")]
        public double Parameter2
        {
            get { return (double)GetValue(Parameter2Property); }
            set { SetValue(Parameter2Property, value); }
        }
        public static readonly DependencyProperty Parameter2Property =
            DependencyProperty.Register("Parameter2", typeof(double), typeof(DCShape), new UIPropertyMetadata(0.9, new PropertyChangedCallback(
                delegate(DependencyObject obj, DependencyPropertyChangedEventArgs e)
                {
                    DCShape shape = obj as DCShape;
                    if (shape != null)
                        shape.UpdateAll(false);
                }
                )));


        // parametro 3
        [DisplayName("Parametro C")]
        public double Parameter3
        {
            get { return (double)GetValue(Parameter3Property); }
            set { SetValue(Parameter3Property, value); }
        }
        public static readonly DependencyProperty Parameter3Property =
            DependencyProperty.Register("Parameter3", typeof(double), typeof(DCShape), new UIPropertyMetadata(0.9, new PropertyChangedCallback(
                delegate(DependencyObject obj, DependencyPropertyChangedEventArgs e)
                {
                    DCShape shape = obj as DCShape;
                    if (shape != null)
                        shape.UpdateAll(false);
                }
                )));

        /// <summary>
        /// Mostra o nasconde le misure della geometria
        /// </summary>
        [DisplayName("Mostra misura")]
        public bool ShowMeasureEnabled
        {
            get { return (bool)GetValue(ShowMeasureEnabledProperty); }
            set { SetValue(ShowMeasureEnabledProperty, value); }
        }
        public static readonly DependencyProperty ShowMeasureEnabledProperty =
            DependencyProperty.Register("ShowMeasureEnabledProperty", typeof(bool), typeof(DCShape), new UIPropertyMetadata(true, new PropertyChangedCallback(
                delegate(DependencyObject obj, DependencyPropertyChangedEventArgs e)
                {
                    DCShape shape = obj as DCShape;
                    if (shape != null)
                        shape.UpdateAll(false);
                }
                )));

        #endregion


        /// <summary>
        /// Ottiene la geometria 2D
        /// </summary>
        protected override Geometry DefiningGeometry
        {
            get
            {
                if(DCGeometryMngr.GetGeometry(this))
                    return _geometry;

                return null;
            }
        }

        /// <summary>
        /// Setta il primo punto della geometria nella posizione indicata
        /// </summary>
        /// <param name="startPoint"></param>
        public void SetStartPoint(Point startPoint)
        {
            Debug.Assert(startPoint != null);
            if (startPoint == null)
                throw new ArgumentNullException("startPoint");

            Position = startPoint.GetClone();
            Points[0] = DCUtils.ZeroPt();
        }

        public int GetPointCount()
        {
            return Points.Count;
        }

        public Point GetAbsolutePoint(int index)
        {
            // valido indice
            Debug.Assert(index >= 0);
            if (index < 0)
                throw new ArgumentOutOfRangeException("index");

            Point point = Points[index];
            return new Point(point.X + Position.X, point.Y + Position.Y);
        }

        public void SetFromAbsolutePoint(int index, Point absolutePoint)
        {
            // valido indice
            Debug.Assert(index >= 0);
            if (index < 0)
                throw new ArgumentOutOfRangeException("index");

            // valido indice
            Debug.Assert(absolutePoint != null);
            if (absolutePoint == null)
                throw new ArgumentNullException("absolutePoint");

            Points[index] = new Point(absolutePoint.X - Position.X, absolutePoint.Y - Position.Y);
        }

        /// <summary>
        /// Rimuove un punto dalla geometria
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool RemovePoint(int index)
        {
            // si può rimuovere solo se i punti sono maggiori a 2
            if (Points.Count == 2)
                return false;

            // valido indice
            Debug.Assert(index >= 0);
            if (index < 0)
                throw new ArgumentOutOfRangeException("index");

            this.Points.RemoveAt(index);

            this.UpdateAll(true, true);

            return true;
        }




        // handling eventi
        protected override void OnMouseEnter(MouseEventArgs e) { DCCore.OnEnter(this); }
        protected override void OnMouseLeave(MouseEventArgs e) { DCCore.OnLeave(this); }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            // recupera canvas
            DCCanvas canvas = GetCanvas();

            // valida canvas
            Debug.Assert(canvas != null);
            if (canvas == null)
                throw new ArgumentNullException("canvas");

            // effetto hover
            if (IsMouseOver && !IsSystemEntity)
                dc.DrawGeometry(Brushes.Transparent, new Pen(Brushes.Blue, 2), _geometry);

            // effetto fuori
            if (Position.X >= canvas.SheetWidth || Position.Y >= canvas.SheetHeight)
                dc.DrawGeometry(canvas.Background, new Pen(Brushes.DimGray, this.StrokeThickness), _geometry);

            // impostazioni di disegno avanzate
            /*if (ShapeType == DCShapeType.Rectangle && RenderImage != null)
            {
                var bounds=GetBounds();
                dc.DrawImage(RenderImage, new Rect(0,0,bounds.Width,bounds.Height));
            }
            else
             */
            if (ShapeType == DCShapeType.Ellipse && Selected)
            {
                dc.DrawEllipse(Brushes.DimGray, canvas.DefaultPen, Points[0], 3, 3);
                dc.DrawLine(canvas.DefaultPen, Points[0], Points[1]);
            }
            else if (ShapeType == DCShapeType.Bezier && Selected)
            {
                for (int i = 0; i < Points.Count - 1; i++)
                {
                    dc.DrawLine(canvas.DefaultPen, Points[i], Points[i + 1]);
                }
            }
            else if (ShapeType == DCShapeType.Arrow && Selected)
            {
                for (int i = 0; i < Points.Count - 1; i++)
                    dc.DrawLine(canvas.DefaultPen, Points[i], Points[i + 1]);
            }

            // disegno tracciato originale se ho una scala
            if (Selected)
            {
                if (_scaleTransform.ScaleX != 1 || _scaleTransform.ScaleY != 1)
                {
                    for (int i = 0; i < Points.Count - 1; i++)
                        dc.DrawLine(new Pen(Brushes.DarkBlue, 1), DCUtils.GetScaledPoint(Points[i], _scaleTransform.ScaleX, _scaleTransform.ScaleY), DCUtils.GetScaledPoint(Points[i + 1], _scaleTransform.ScaleX, _scaleTransform.ScaleY));
                }
            }

            // disegna perimetro e area rapportato all'unità di misura
            if (ShowMeasureEnabled)
            {
                string str = canvas.GetMeasure(DCMathUtils.GetPerimeter(Points, ShapeType)) + "\n" + "A: " + canvas.GetMeasure(DCMathUtils.GetArea(Points, ShapeType));
                dc.DrawText(str, Points[0]);
            }

            // fa scattare l'evento di render
            if (RenderEvent != null)
                RenderEvent(dc);
        }
        #endregion


        #region ICloneable Inerface
        /// <summary>
        /// Crea un clone
        /// </summary>
        /// <returns></returns>
        public virtual object Clone()
        {
            // crea lo shape
            DCShape shape = new DCShape();
            
            // recupera le proprietà
            shape.Fill = Fill.Clone();
            shape.Stroke = Stroke.Clone();
            shape.StrokeThickness = StrokeThickness;
            shape.ShapeType = ShapeType;
            shape.Position = Position.GetClone();
            shape.Scale = Scale.GetClone();
            shape.Id = Id;
            shape.Smooth = Smooth;
            shape.Parameter1 = Parameter1;
            shape.Parameter2 = Parameter2;
            shape.Parameter3 = Parameter3;
            
            if(shape.Effect!=null)
                shape.Effect = this.Effect.Clone();

            PointCollection points = new PointCollection();
            foreach (Point p in Points)
                points.Add(p.GetClone());

            shape.Points = points;

            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            foreach (string key in Data.Keys)
                dictionary.Add(key, Data[key]);

            shape.Data = dictionary;

            return shape;
        }
        #endregion
    }
}
