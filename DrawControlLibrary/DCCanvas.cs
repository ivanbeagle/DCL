﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using DrawControlLibrary.Entities;
using DrawControlLibrary.Managers;
using System.Windows.Documents;

namespace DrawControlLibrary
{
    /// <summary>
    /// Canvas esteso
    /// </summary>
    public partial class DCCanvas : System.Windows.Controls.Canvas , INotifyPropertyChanged
    {
        // adorners
        internal AdornerLayer _adornerLayer;

        // variabili unità di misura
        private bool _enableMeasureReference = true;
        private double _measureReference = 10;
        private string _measureReferenceCaption = "cm";

        // TODO: manager di undo redo

        // variabili di foglio
        private Brush _sheetColor;
        private double _sheetBoxWidth;
        private double _sheetBoxHeight;

        internal bool _enableSelectionBox=true;                 // abilitare la selezione rettangolare?
        internal bool _captured;                                // indica che ha ricevuto il click
        internal bool _selection;                               // indica che c'è una selezione in atto(dipende dalla visibilità del box)
        internal Point _oldMousePosition;                       // posizione mouse

        internal Rectangle _selectionBox;                       // rettangolo di selezione
        internal Thumb _sheetBoxControl;                        // controllo per il foglio
        internal IDCEntity _lastCapturedEntity;                 // ultima entity catturata
        internal DCAbstractTool _lastTool;                      // tool attivo
        internal List<DCControlPoint> _cacheControlPoints;      // cache punti di controllo
        internal ContextMenu _contextMenu;                      // menu contestuale
        internal Image _background;                             // background

        // primitive usate dai tool
        public DCShape ToolShape_1  { get; internal set; }
        public DCShape ToolShape_2  { get; internal set; }
        public DCShape ToolShape_3  { get; internal set; }
        public DCShape ToolShape_4  { get; internal set; }
        public DCShape ToolShape_5  { get; internal set; }
        public Label Prompt         { get; internal set; }

        // a chi notifica le coordinate e la dimensione effettiva del foglio?
        internal Label _coordinatesLbl;
        internal Label _sheetSizeLbl;

        // penna
        public Pen DefaultPen = new Pen(Brushes.DimGray, 1);

        /// <summary>
        /// Ottiene o imposta il colore del foglio
        /// </summary>
        public Brush SheetColor
        {
            get { return _sheetColor; }
            set
            {
                _sheetColor = value;

                // rigenera il disegno
                Regen(true);

                FirePropertyChanged("SheetColor");
            }
        }

        /// <summary>
        /// Ottiene o imposta la larghezza del foglio
        /// </summary>
        public double SheetWidth
        {
            get { return _sheetBoxWidth; }
            set
            {
                _sheetBoxWidth = value;

                // muove il punto di controllo associato
                UpdateSheetBox();

                // rigenera il disegno
                Regen(true);

                // lancia evento
                if (SheetSizeChanged != null)
                    SheetSizeChanged(this, new DCSheetEventArgs(SheetWidth, SheetHeight));

                FirePropertyChanged("SheetWidth");
            }
        }

        /// <summary>
        /// Ottiene o imposta l'altezza del foglio
        /// </summary>
        public double SheetHeight
        {
            get { return _sheetBoxHeight; }
            set
            {
                // lo stesso per la larghezza vale per l'altezza
                _sheetBoxHeight = value;

                // muove il punto di controllo associato
                UpdateSheetBox();

                // rigenera il disegno
                Regen(true);

                if (SheetSizeChanged != null)
                    SheetSizeChanged(this, new DCSheetEventArgs(SheetWidth, SheetHeight));

                FirePropertyChanged("SheetHeight");
            }
        }

        /// <summary>
        /// Restituisce vero se c'è un tool attivo
        /// </summary>
        public bool HasToolActived
        {
            get { return _lastTool != null ? true : false; }
        }

        /// <summary>
        /// Restituisce vero se il background è caricato e visibile
        /// </summary>
        public bool HasBackgroundEnabled
        {
            get
            {
                if (_background.Source == null)
                    return false;
                if (_background.Visibility == System.Windows.Visibility.Hidden)
                    return false;

                return true;
            }
        }

        /// <summary>
        /// Ottiene l'ultima entità catturata da un operazione
        /// </summary>
        public IDCEntity LastCapturedEntity
        {
            get { return _lastCapturedEntity; }
        }

        /// <summary>
        /// Ottiene o imposta lo Zoom del canvas
        /// </summary>
        public double Zoom
        {
            get
            {
                ScaleTransform scale = this.LayoutTransform as ScaleTransform;
                
                // se è null lo instanzia e lo richiama
                if (scale == null)
                {
                    this.LayoutTransform = new ScaleTransform(1, 1, 0, 0);
                    return Zoom;
                }

                return scale.ScaleX; // scaleX == scaleY
            }
            set
            {
                ScaleTransform scale = this.LayoutTransform as ScaleTransform;
                scale.ScaleX = value;
                scale.ScaleY = value;

                // rigenera il disegno
                Regen(true);

                FirePropertyChanged("Zoom");
            }
        }

        /// <summary>
        /// Ottiene la selezione
        /// </summary>
        public List<IDCEntity> Selection { get; private set; }

        /// <summary>
        /// Abilita la misura di riferimento
        /// </summary>
        public bool EnableMeasureReference
        {
            get{ return _enableMeasureReference; }
            set
            { 
                _enableMeasureReference = value;

                // rigenera le entità solo
                RefreshEntities();

                FirePropertyChanged("EnableMeasureReference");
            }
        }

        /// <summary>
        /// Valore della misura di riferimento
        /// </summary>
        public double MeasureReference
        {
            get{ return _measureReference; }
            set
            {
                if (value != 0)
                {
                    _measureReference = value;

                    // rigenera le entità solo
                    RefreshEntities();

                    FirePropertyChanged("MeasureReference");
                }
                else
                    MessageBox.Show("Il valore della proprietà MeasureReference non può essere 0", "DrawControlLibrary");
            }
        }

        /// <summary>
        /// Ottiene o imposta il nome della misura di riferimento
        /// </summary>
        public string MeasureReferenceCaption
        {
            get { return _measureReferenceCaption; }
            set
            {
                _measureReferenceCaption = value;

                // rigenera le entità solo
                RefreshEntities();

                FirePropertyChanged("MeasureReferenceCaption");
            }
        }

        /// <summary>
        /// Questo evento viene chiamato quando un entità viene aggiunta nel disegno
        /// </summary>
        public event EventHandler EntityAdded;

        /// <summary>
        /// Questo evento viene chiamato quando un entità viene rimossa dal disegno
        /// </summary>
        public event EventHandler EntityRemoved;

        /// <summary>
        /// Questo evento viene chiamato quando cambia la selezione
        /// </summary>
        public event EventHandler SelectionChanged;

        /// <summary>
        /// Questo evento viene chiamato quando si verifica una selezione rettangolare
        /// </summary>
        public event EventHandler<DCSelectionEventArgs> RectangularSelection;

        /// <summary>
        /// Questo evento viene chiamato quando la dimensione del foglio cambia
        /// </summary>
        public event EventHandler<DCSheetEventArgs> SheetSizeChanged;


        /// <summary>
        /// Costruttore
        /// </summary>
        public DCCanvas() : base()
        {
            // agisce sulle prestazioni
            //RenderOptions.SetCachingHint(this, CachingHint.Cache);
            //RenderOptions.SetCacheInvalidationThresholdMinimum(this, 3);
            //RenderOptions.SetCacheInvalidationThresholdMaximum(this, 6);
            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.NearestNeighbor);
            RenderOptions.SetEdgeMode(this, EdgeMode.Unspecified);

            // colore di default del canvas
            Background = Brushes.LightGray;

            // inizializzazione altri oggetti
            Selection = new List<IDCEntity>();

            // inizializza cache
            _cacheControlPoints = new List<DCControlPoint>();

            // ordine di inizializzazione dei vari oggetti
            
            _initContextMenu();         // 1 - inizializza il menu contestuale
            _initVirtualGrid();         // 2 - griglia virtuale, serve per captare il mouse
            _initSheet();               // 3 - imposta le dimensioni del foglio e del controllo per ridimensionare il foglio
            _initSelectionBox();        // 4 - box di selezione
            _initBackground();          // 5 - inizializza il background
            _initBasicPrimitives(true); // 6 - inizializza le primitive di base

            // disabilità l'effetto puntinato del focus
            FocusVisualStyle = null;

            // supporta la tastiera
            Focusable = true;

            // supporta il drop di elementi
            AllowDrop = true;

            Loaded += DCCanvas_Loaded;
        }


        #region Eventi
        void DCCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // ricava l'adorner per l'applicazione di eventuali oggetti
            _adornerLayer = AdornerLayer.GetAdornerLayer(this);
        }
        #endregion


        #region Inizializzazioni
        private void _initContextMenu()
        {
            _contextMenu=new ContextMenu();
            _contextMenu.Opacity = 0.95;
            //_contextMenu.Background = Brushes.WhiteSmoke;
            //_contextMenu.BorderBrush = Brushes.Gray;
            //_contextMenu.BorderThickness = new Thickness(1);
            _contextMenu.HasDropShadow=false;
            _contextMenu.StaysOpen = true;
            _contextMenu.Opened += new RoutedEventHandler(DCCommandContextMngr.OnMenuOpen);

            new DCMenuItem("Porta in basso", SendToBack, _contextMenu, DCDefinitions.IMAGE_PATH + "send_back.png", true, false);
            new DCMenuItem("Porta indietro", MoveBack, _contextMenu, DCDefinitions.IMAGE_PATH + "send_back.png", true, false);
            new DCMenuItem("Porta avanti", MoveNext, _contextMenu, DCDefinitions.IMAGE_PATH + "bring_front.png");
            new DCMenuItem("Porta in primo piano", BringToFront, _contextMenu, DCDefinitions.IMAGE_PATH + "bring_front.png");
            new DCMenuItem("Capovolgi orizzontalmente", delegate(IDCEntity entity){ entity.Scale = new Point(-entity.Scale.X, entity.Scale.Y); }, _contextMenu, DCDefinitions.IMAGE_PATH + "flip_horizontal.png");
            new DCMenuItem("Capovolgi verticalmente", delegate(IDCEntity entity) { entity.Scale = new Point(entity.Scale.X, -entity.Scale.Y); }, _contextMenu, DCDefinitions.IMAGE_PATH + "flip_vertical.png");

            #region rasterizzazione
            DCMenuItem rasterize = new DCMenuItem("Rasterizza",  delegate(IDCEntity entity)
                {
                    DCImage image = entity.GetRaster();
                    if(image!=null)
                        AddEntity(image);

                }, _contextMenu, DCDefinitions.IMAGE_PATH + "raster.png");
            #endregion

            #region duplicazione
            DCMenuItem duplicate = new DCMenuItem("Duplica", delegate(IDCEntity entity)
            {
                AddEntity((IDCEntity)entity.Clone());

            }, _contextMenu, DCDefinitions.IMAGE_PATH + "duplicate.png");
            #endregion

            DCMenuItem remove = new DCMenuItem("Rimuovi", RemoveEntity, _contextMenu, DCDefinitions.IMAGE_PATH + "delete.png", false);
                remove.Name = DCDefinitions.CONTEXT_REMOVE;
        }

        private void _initVirtualGrid()
        {
            // inizializza griglia virtuale per il controllo del mouse
            Grid grid = new Grid();
            grid.Visibility = Visibility.Visible;
            grid.Background = Brushes.Transparent;
            Binding bindWidth = new Binding("ActualWidth");
            bindWidth.Source = this;
            Binding bindHeight = new Binding("ActualHeight");
            bindHeight.Source = this;
            grid.SetBinding(Grid.WidthProperty, bindWidth);
            grid.SetBinding(Grid.HeightProperty, bindHeight);
            Children.Add(grid);
        }

        private void _initSheet()
        {
            double w = 1024;
            double h = 768;
            
            // crea un punto di controllo che permette di ridimensionare il foglio
            _sheetBoxControl = new Thumb();
            _sheetBoxControl.Focusable = true;
            _sheetBoxControl.Cursor = Cursors.SizeNWSE;
            _sheetBoxControl.DragDelta += new DragDeltaEventHandler(sheetBoxControl_DragDelta);
            _sheetBoxControl.Width = 8;
            _sheetBoxControl.Height = 8;
            _sheetBoxControl.BorderBrush = Brushes.Black;
            _sheetBoxControl.BorderThickness = new Thickness(1);
            DCCanvas.SetLeft(_sheetBoxControl, w);
            DCCanvas.SetTop(_sheetBoxControl, h);
            Children.Add(_sheetBoxControl);

            // setto dimensioni del foglio
            _sheetBoxWidth = w;
            _sheetBoxHeight = h;

            // setto colore del foglio
            SheetColor = Brushes.White;
        }

        private void _initSelectionBox()
        {
            // crea il rettangolo di selezione
            _selectionBox = new Rectangle();
            _selectionBox.Visibility = Visibility.Hidden;
            _selectionBox.Fill = Brushes.WhiteSmoke;
            _selectionBox.StrokeThickness = 1.5; // BUG: Occorre una misura >1 per consentire la "selezione" su click di un oggetto DCImage
            _selectionBox.Stroke = Brushes.Gray;
            //_selectionBox.StrokeDashArray = new DoubleCollection();
            //_selectionBox.StrokeDashArray.Add(3);
            _selectionBox.Opacity = 0.65;
        }

        private void _initBackground()
        {
            // inizializza lo sfondo
            _background = new Image();
            _background.Source = null;
            _background.Visibility = System.Windows.Visibility.Hidden;
            DCCanvas.SetLeft(_background, 0);
            DCCanvas.SetTop(_background, 0);
            Children.Add(_background);
        }

        private void _initBasicPrimitives(bool enableMeasure = false)
        {
            ToolShape_1 = new DCShape();
            ToolShape_1.IsSystemEntity = true;
            ToolShape_1.Editable = false;
            ToolShape_1.Deletable = false;
            ToolShape_1.Selectable = false;
            ToolShape_1.ShowMeasureEnabled = enableMeasure;
            // necessari 2 punti iniziali altrimenti crash in measureoverride
            ToolShape_1.Points.Add(new Point(0, 0));
            ToolShape_1.Points.Add(new Point(0, 0));

            ToolShape_2 = new DCShape();
            ToolShape_2.IsSystemEntity = true;
            ToolShape_2.Editable = false;
            ToolShape_2.Deletable = false;
            ToolShape_2.Selectable = false;
            ToolShape_2.ShowMeasureEnabled = enableMeasure;
            ToolShape_2.Points.Add(new Point(0, 0));
            ToolShape_2.Points.Add(new Point(0, 0));

            ToolShape_3 = new DCShape();
            ToolShape_3.IsSystemEntity = true;
            ToolShape_3.Editable = false;
            ToolShape_3.Deletable = false;
            ToolShape_3.Selectable = false;
            ToolShape_3.ShowMeasureEnabled = enableMeasure;
            ToolShape_3.Points.Add(new Point(0, 0));
            ToolShape_3.Points.Add(new Point(0, 0));

            ToolShape_4 = new DCShape();
            ToolShape_4.IsSystemEntity = true;
            ToolShape_4.Editable = false;
            ToolShape_4.Deletable = false;
            ToolShape_4.Selectable = false;
            ToolShape_4.ShowMeasureEnabled = enableMeasure;
            ToolShape_4.Points.Add(new Point(0, 0));
            ToolShape_4.Points.Add(new Point(0, 0));

            ToolShape_5 = new DCShape();
            ToolShape_5.IsSystemEntity = true;
            ToolShape_5.Editable = false;
            ToolShape_5.Deletable = false;
            ToolShape_5.Selectable = false;
            ToolShape_5.ShowMeasureEnabled = enableMeasure;
            ToolShape_5.Points.Add(new Point(0, 0));
            ToolShape_5.Points.Add(new Point(0, 0));

            Prompt = new Label();
            Prompt.Style = null;
            Prompt.Foreground = Brushes.Black;
            Prompt.Background = Brushes.WhiteSmoke;
            Prompt.BorderBrush = Brushes.Gray;
            Prompt.BorderThickness = new Thickness(1);
            Prompt.Content = "";
            Prompt.Opacity = 0.95;
        }
        #endregion


        #region Controllo del foglio
        // gestiscono la dimensione del foglio e l'aggiornamento della caption e punto di controllo
        internal void UpdateSheetBox()
        {
            DCCanvas.SetLeft(_sheetBoxControl, _sheetBoxWidth);
            DCCanvas.SetTop(_sheetBoxControl, _sheetBoxHeight);

            // invia notifiche alla label
            if (_sheetSizeLbl != null)
                _sheetSizeLbl.Content = (int)_sheetBoxWidth + " ; " + (int)_sheetBoxHeight;
        }

        void sheetBoxControl_DragDelta(object sender, DragDeltaEventArgs e)
        {
            double w = _sheetBoxWidth + e.HorizontalChange;
            if (w < 1) w = 1;

            double h = _sheetBoxHeight + e.VerticalChange;
            if (h < 1) h = 1;

            _sheetBoxWidth = w;
            _sheetBoxHeight = h;

            // aggiorna foglio
            UpdateSheetBox();

            // rigenera il disegno
            Regen(true);

            // lancia evento
            if (SheetSizeChanged != null)
                SheetSizeChanged(this, new DCSheetEventArgs(SheetWidth, SheetHeight)); 
        }
        #endregion

       
        #region Gestione della selezione(uso interno)
        internal void AddToSelection(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            Selection.Add(entity);
            if (SelectionChanged != null)
                SelectionChanged(entity, null);
        }

        internal void RemoveFromSelection(IDCEntity entity)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            Selection.Remove(entity);
            if (SelectionChanged != null)
                SelectionChanged(entity, null);
        }
        #endregion


        #region Gestione dell'aggiunta e rimozione (uso interno)
        private void _removeEntity(IDCEntity entity, bool notifyErase, bool isSystemErase)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // deve essere cancellabile(ma se è un operazione di sistema continua lo stesso)
            if (!entity.Deletable && !isSystemErase)
                return;

            // un entità rimossa diventa automaticamente Non selezionata(gli eventi di deselezione non scattano se l'operazione è stata flaggata)
            if (isSystemErase)
                entity.Data[DCDefinitions.FLAG_NOTIFY_DESELECTION] = false;

            if (entity.Selected)
                entity.Selected = false;

            // ripristina il flag dopo averlo usato
            if (isSystemErase)
                entity.Data[DCDefinitions.FLAG_NOTIFY_DESELECTION] = true;

            // tolgo dall'entità catturata se cancello la medesima
            if (_lastCapturedEntity == entity)
                _lastCapturedEntity = null;

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (entity == null)
                throw new ArgumentNullException("element");

            // rimozione
            base.InternalChildren.Remove(element);

            // notifica evento
            if (notifyErase)
            {
                if (EntityRemoved != null)
                    EntityRemoved(entity, null);
            }
        }

        internal void AddEntity(IDCEntity entity, bool notifyAdd)
        {
            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            // recupera elemento ui
            FrameworkElement element = entity as FrameworkElement;

            Debug.Assert(element != null);
            if (entity == null)
                throw new ArgumentNullException("element");

            // aggiunta al canvas
            base.InternalChildren.Add(element);

            // notifica evento
            if (notifyAdd)
            {
                if (EntityAdded != null)
                    EntityAdded(entity, null);
            }
        }
        #endregion


        #region Overrides importanti
        // gestisce il ridimensionamento del canvas
        protected override Size MeasureOverride(Size constraint)
        {
            Size availableSize = new Size(double.PositiveInfinity, double.PositiveInfinity);
            double maxHeight = 0;
            double maxWidth = 0;
            try
            {
                foreach (FrameworkElement element in base.InternalChildren)
                {
                    if (element != null)
                    {
                        element.Measure(availableSize);
                        double left = DCCanvas.GetLeft(element);
                        double top = DCCanvas.GetTop(element);
                        left += element.DesiredSize.Width;
                        top += element.DesiredSize.Height;
                        maxWidth = maxWidth < left ? left : maxWidth;
                        maxHeight = maxHeight < top ? top : maxHeight;
                    }
                }
                return new Size { Height = maxHeight, Width = maxWidth };
            }
            catch
            {
                return new Size(SheetWidth, SheetHeight);
            }
        }

        // override necessari! ( O B B L I G A T O R I )
        protected override int VisualChildrenCount { get { return base.InternalChildren.Count; } }
        protected override Visual GetVisualChild(int index) { return base.InternalChildren[index]; }
        #endregion


        #region Gestione Input

        // drag

        protected override void OnPreviewDragEnter(DragEventArgs e)
        {
            base.OnPreviewDragEnter(e);
            try
            {
                DCCore.OnDragEnter(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewDragLeave(DragEventArgs e)
        {
            base.OnPreviewDragLeave(e);
            try
            {
                DCCore.OnDragLeave(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewDragOver(DragEventArgs e)
        {
            base.OnPreviewDragOver(e);
            try
            {
                DCCore.OnDragOver(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewDrop(DragEventArgs e)
        {
            base.OnPreviewDrop(e);
            try
            {
                DCCore.OnDrop(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        // click, movimento e tastiera

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
            try
            {
                DCCore.OnMouseDown(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseUp(e);
            try
            {
                DCCore.OnMouseUp(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);
            try
            {
                DCCore.OnMouseMove(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            base.OnPreviewKeyUp(e);
            try
            {
                DCCore.OnKeyUp(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);
            try
            {
                DCCore.OnKeyDown(this, e);
            }
            catch (Exception ex) { DCErrorMngr.HandleEx(ex); }
        }
        
        #endregion


        // evento di disegno
        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            // disegno il foglio di lavoro
            _drawSheet(dc);

            // verifico se ho un contesto di TOOL
            if (_lastTool != null)
                _lastTool.OnRender(dc);

            // solo alla fine verifico se rilasciare il tool
            if (_lastTool != null && _lastTool.IsFinished)
                SetTool(null);
        }

        
        // disegna il foglio di lavoro
        private void _drawSheet(DrawingContext dc)
        {
            double halfPenWidth = DefaultPen.Thickness / 2;

            Rect sheet = new Rect(0, 0, _sheetBoxWidth, _sheetBoxHeight);

            GuidelineSet guidelines = new GuidelineSet();
            guidelines.GuidelinesX.Add(sheet.Left + halfPenWidth);
            guidelines.GuidelinesX.Add(sheet.Right + halfPenWidth);
            guidelines.GuidelinesY.Add(sheet.Top + halfPenWidth);
            guidelines.GuidelinesY.Add(sheet.Bottom + halfPenWidth);

            dc.PushGuidelineSet(guidelines);
            dc.DrawRectangle(SheetColor, null, sheet);
            dc.Pop();

            // se abilitata disegna l'unità di misura
            if (EnableMeasureReference)
            {
                double baseX = _sheetBoxWidth + 15;
                double baseY = _sheetBoxHeight + 15;
                dc.DrawText("u", new Point(baseX, baseY + 2), 10);
                dc.DrawLine(new Pen(Brushes.Blue, 1), new Point(baseX + 10, baseY + 5), new Point(baseX + 10 + MeasureReference, baseY + 5));
                dc.DrawText(String.Format("{0:0.00}", MeasureReference) + " px = 1 " + MeasureReferenceCaption, new Point(baseX + 10 + MeasureReference + 5, baseY), 10);
            }
        }


        // provvede alla generazione dell'evento di selezione rettangolare
        internal void FireRectangularSelectionEvent(System.Drawing.RectangleF rectArea)
        {
            if (RectangularSelection != null)
                RectangularSelection(this, new DCSelectionEventArgs(DCSelectionType.Rectangular, rectArea));
        }


        #region Interfaccia INotify
        public event PropertyChangedEventHandler PropertyChanged;
        public void FirePropertyChanged(string propertyName)
        {
            try
            {
                Debug.Assert(!String.IsNullOrEmpty(propertyName));
                if (String.IsNullOrEmpty(propertyName))
                    throw new ArgumentNullException("propertyName");

                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                DCErrorMngr.HandleEx(ex);
            }
        }
        #endregion

    }
}
