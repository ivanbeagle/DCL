﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;

namespace DrawControlLibrary
{
    /// <summary>
    /// Metodi di supporto, utility e di estensioni ad uso interno
    /// </summary>
    internal static class DCSupport
    {
        internal static string GetDataStr(Dictionary<string, object> data)
        {
            if (data == null)
                return string.Empty;

            string strData = string.Empty;

            for (int i = 0; i < data.Count; i++)
            {
                // ignora i data di sistema "_"
                string key = data.ElementAt(i).Key;
                if (key.StartsWith("_"))
                    continue;

                // pulisco chiave e valore da caratteri ; e =
                key = key.Trim(';', '=');

                string value = data.ElementAt(i).Value.ToString();
                value = value.Trim(';', '=');

                strData += string.Format("{0}={1};", key, value);
            }

            // rimuove finale ;
            if(strData.EndsWith(";"))
                strData=strData.TrimEnd(';');

            return strData;
        }

        internal static Point? GetPoint(string strPoint)
        {
            bool isNull=String.IsNullOrEmpty(strPoint);
            if (isNull)
                return null;
            
            string[] tokens = strPoint.Split(',');
            try
            {
                return new Point(
                    Convert.ToDouble(tokens[0], DCDefinitions.NumberFormat),
                    Convert.ToDouble(tokens[1], DCDefinitions.NumberFormat)
                    );
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // formato: x;y x2;y2; x3;y3; ...
        internal static PointCollection GetPointCollection(string strPoints)
        {
            bool isNull = String.IsNullOrEmpty(strPoints);
            if (isNull)
                return null;
           
            string[] tokens = strPoints.Split(' ');
            try
            {
                PointCollection collection=new PointCollection();
                foreach (string strPoint in tokens)
                    collection.Add(GetPoint(strPoint).Value);
                return collection;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        internal static BezierSegment PolygonSegment(Vector point0, Vector point1, Vector point2, Vector point3, double smoothValue)
        {
            Debug.Assert(point0 != null);
            if (point0 == null)
                return null;
            Debug.Assert(point1 != null);
            if (point1 == null)
                return null;
            Debug.Assert(point2 != null);
            if (point2 == null)
                return null;
            Debug.Assert(point3 != null);
            if (point3 == null)
                return null;

            var c1 = (point0 + point1) / 2d;
            var c2 = (point1 + point2) / 2d;
            var c3 = (point2 + point3) / 2d;

            var len1 = (point1 - point0).Length;
            var len2 = (point2 - point1).Length;
            var len3 = (point3 - point2).Length;

            var k1 = len1 / (len1 + len2);
            var k2 = len2 / (len2 + len3);

            var m1 = c1 + (c2 - c1) * k1;
            var m2 = c2 + (c3 - c2) * k2;

            var ctrl1 = m1 + (c2 - m1) * smoothValue + point1 - m1;
            var ctrl2 = m2 + (c2 - m2) * smoothValue + point2 - m2;

            var curve = new BezierSegment();
                curve.Point1 = (Point)ctrl1;
                curve.Point2 = (Point)ctrl2;
                curve.Point3 = (Point)point2;

            return curve;
        }

        internal static void PutData(string dataStr, IDCEntity entity)
        {
            if (string.IsNullOrEmpty(dataStr))
                return;

            Debug.Assert(entity != null);
            if (entity == null)
                throw new ArgumentNullException("entity");

            const int KEY=0;
            const int VALUE=1;

            // formato dati: key=value;key2=value2;...
            string[] tokens = dataStr.Split(';');
            if(tokens!=null)
                foreach (string token in tokens)
                {
                    string[] kv = token.Split('=');
                    if (kv == null || kv.Count() != 2)
                        continue;

                    if (entity.Data == null)
                        entity.Data = new Dictionary<string, object>();

                    entity.Data.Add(kv[KEY], kv[VALUE]);
                }
        }

        internal static Thickness GetBorderThickness(string strValue)
        {
            try
            {
                if (string.IsNullOrEmpty(strValue))
                    return new Thickness(0);

                string[] tokens = strValue.Split(',');
                if (tokens.Length != 4)
                    return new Thickness(0);

                double t1 = Convert.ToDouble(tokens[0], DCDefinitions.NumberFormat);
                double t2 = Convert.ToDouble(tokens[1], DCDefinitions.NumberFormat);
                double t3 = Convert.ToDouble(tokens[2], DCDefinitions.NumberFormat);
                double t4 = Convert.ToDouble(tokens[3], DCDefinitions.NumberFormat);

                return new Thickness(t1, t2, t3, t4);
            }
            catch (Exception ex)
            {
                return new Thickness();
            }
        }

        internal static RenderTargetBitmap RenderToBitmap(this IDCEntity entity)
        {
            //Debug.Assert(entity != null);
            //if (entity == null)
            //    return null;

            // ottiene l'elemento wpf
            FrameworkElement element = entity as FrameworkElement;
            Debug.Assert(element != null);
            if (element == null)
                return null;

            try
            {
                // ottiene il bound
                var bounds = entity.GetBounds();
                // var bounds = VisualTreeHelper.GetDescendantBounds(element);

                // crea una bitmap
                var rtb = new RenderTargetBitmap((int)(bounds.Width), (int)(bounds.Height), DCUtils.DPIX, DCUtils.DPIY, PixelFormats.Pbgra32);
                
                // crea la visuale di disegno
                DrawingVisual drawingVisual = new DrawingVisual();

                using (DrawingContext drawingContext = drawingVisual.RenderOpen())
                {
                    VisualBrush vbrush = new VisualBrush(element);
                    drawingContext.DrawRectangle(vbrush, null, new Rect(0, 0, bounds.Width, bounds.Height));
                }

                // render
                rtb.Render(drawingVisual);

                return rtb;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static Effect GetEffectObject(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    value = value.Replace("&lt;", "<");
                    value = value.Replace("&gt;", ">");
                    return (Effect)XamlReader.Parse(value);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        internal static string GetEffectStr(Effect fx)
        {
            string strFx = string.Empty;

            if (fx == null)
                return strFx;

            try
            {
                strFx = XamlWriter.Save(fx);
                strFx = strFx.Replace("<", "&lt;");
                strFx = strFx.Replace(">", "&gt;");
            }
            catch (Exception ex)
            {
                strFx = string.Empty;
            }

            return strFx;
        }

        internal static Brush GetBrushObject(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    value = value.Replace("&lt;", "<");
                    value = value.Replace("&gt;", ">");
                    return (Brush)XamlReader.Parse(value);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        internal static string GetBrushStr(Brush brush)
        {
            string strBrush = string.Empty;

            if (brush == null)
                return strBrush;

            try
            {
                strBrush = XamlWriter.Save(brush);
                strBrush = strBrush.Replace("<", "&lt;");
                strBrush = strBrush.Replace(">", "&gt;");
            }
            catch (Exception ex)
            {
                strBrush = string.Empty;
            }

            return strBrush;
        }

        internal static Point? GetPosition(this IDCEntity entity)
        {
            FrameworkElement element = entity as FrameworkElement;
            Debug.Assert(element != null);
            if (element == null)
                return null;

            Point p = new Point();
                p.X = DCCanvas.GetLeft(element);
                p.Y = DCCanvas.GetTop(element);

            return p;
        }

        internal static void DrawText(this DrawingContext dc, string text, Point point, double fontSize = 12)
        {
            dc.DrawText(new FormattedText(text, System.Globalization.CultureInfo.InvariantCulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Arial"), fontSize, Brushes.Black), point);
        }
    }
}
