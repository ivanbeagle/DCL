﻿using System.Globalization;

namespace DrawControlLibrary
{
    /// <summary>
    /// Contiene costanti e variabili speciali
    /// </summary>
    public static class DCDefinitions
    {
        internal const int FIRST_INDEX = 3;  // ordine: grid ; background ; elemento

        internal const string KEY_SYS_DRAGGING = "_sysDragging";
        internal const string KEY_DRAGGING = "_dragging";
        internal const string KEY_CAPTURED = "_captured";
        internal const string KEY_MOUSE_OVER = "_mouseOver";
        internal const string KEY_OLD_MOUSE_POSITION = "_oldMousePosition";
        internal const string KEY_BLINKED = "_blinked";
        internal const string KEY_OLD_BACKGROUND = "_oldBackground";
        internal const string KEY_OLD_BACKGROUND_2 = "_oldBackground2";
        internal const string KEY_OLD_FOREGROUND = "_oldForeground";
        internal const string FLAG_NOTIFY_DESELECTION = "_notifyDeselection";
        internal const string CONTEXT_REMOVE = "_contextRemove";
        internal const string IMAGE_PATH = "pack://application:,,,/DrawControlLibrary;component/Images/";

        public const string DRAG_FORMAT_EFFECT = "_formatEffect";
        public const string DRAG_FORMAT_CLIP = "_formatClip";

        // uso futuro?
        public const string DRAG_FORMAT_BACKGROUND = "_formatBackground";
        public const string DRAG_FORMAT_ENTITY = "_formatEntity";

        internal static NumberFormatInfo NumberFormat { get; private set; }

        static DCDefinitions()
        {
            NumberFormat = new NumberFormatInfo();
            NumberFormat.NumberDecimalSeparator = ".";
        }
    }
}
