﻿using System;
using System.Windows;

namespace DrawControlLibrary
{
    /// <summary>
    /// Struttura EventArgs che contiene informazioni sul foglio
    /// </summary>
    public class DCSheetEventArgs : EventArgs
    {
        Size _sheetSize;
        public Size SheetSize { get { return _sheetSize; } }

        public DCSheetEventArgs(double w, double h)
        {
            _sheetSize = new Size(w, h);
        }
    }
}
