﻿using System;

namespace DrawControlLibrary
{
    /// <summary>
    /// Struttura EventArgs quando si manipolano punti di controllo
    /// </summary>
    public class DCEditEventArgs : EventArgs
    {
        int _pointIndex;
        public int PointIndex { get { return _pointIndex; } }

        public DCEditEventArgs(int pointIndex)
        {
            _pointIndex = pointIndex;
        }
    }
}
