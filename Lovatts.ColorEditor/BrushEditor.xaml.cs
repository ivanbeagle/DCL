﻿using System.Windows.Controls;
using BrushEditor;
using System.Windows.Media;
using System.Windows;
using System.ComponentModel;
using DrawControlLibrary;
using DrawControlLibrary.Entities;
using System.Windows.Controls.Primitives;
using System;

namespace Lovatts.ColorEditor
{
    public class BrushEditorEventArgs : EventArgs
    {
        public Brush Brush { get; internal set; }
    }

    public partial class BrushEditor : UserControl , INotifyPropertyChanged
    {
        private BrushEditorViewModel _brushEditorViewModel;
        public event EventHandler<BrushEditorEventArgs> BrushChanged;

        public BrushEditor()
        {
            InitializeComponent();
                BrushEditorViewModel = new BrushEditorViewModel();
                BrushEditorViewModel.ColorEditorViewModel = colorEditor.ColorEditorViewModel;
                BrushEditorViewModel.PropertyChanged += new PropertyChangedEventHandler(BrushEditorViewModel_PropertyChanged);
        }

        void BrushEditorViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (BrushChanged != null)
            {
                BrushEditorEventArgs brushE = new BrushEditorEventArgs();
                brushE.Brush = BrushEditorViewModel.Brush;
                BrushChanged(this, brushE);
            }
        }

        public BrushEditorViewModel BrushEditorViewModel
        {
            get { return _brushEditorViewModel; }
            set 
            { 
                _brushEditorViewModel = value;
                this.DataContext = _brushEditorViewModel;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void FirePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private Point CalcPosition1Based(Thumb thumb, DragDeltaEventArgs e)
        {
            var cx = Canvas.GetLeft(thumb) + e.HorizontalChange;
            var cy = Canvas.GetTop(thumb) + e.VerticalChange;
            return CalcPosition1Based(cx, cy);
        }

        private Point CalcPosition1Based(double cx, double cy)
        {
            var a = brushViewer.ActualWidth;
            var b = 1;

            if (cx < 0)
                cx = 0;
            if (cx > a - 10)
                cx = a - 10;
            if (cy < 0)
                cy = 0;
            if (cy > a - 10)
                cy = a - 10;

            var x = Math.Round(cx / a, 5);
            var y = Math.Round(cy / a, 5);

            return new Point(x, y);
        }

        private void radialCenter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Point p = CalcPosition1Based(radialCenter, e);
            BrushEditorViewModel.Center = p;
        }

        private void linearStartPoint_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Point p = CalcPosition1Based(linearStartPoint, e);
            BrushEditorViewModel.StartPoint = p;
        }

        private void Canvas_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Point position = e.GetPosition(brushViewer);
            position = CalcPosition1Based(position.X-5, position.Y-5);

            if (BrushEditorViewModel.BrushType == BrushTypes.Linear)
                BrushEditorViewModel.StartPoint = position;
            else if(BrushEditorViewModel.BrushType == BrushTypes.Radial)
                BrushEditorViewModel.Center = position;
        }

    }
}